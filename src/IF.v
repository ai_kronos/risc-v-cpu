module ins_fetch #(
    parameter ADD_WIDTH = 18
) (
    input                       clk,
    input                       rst,
    input                       en,
    input   [ADD_WIDTH-1 : 0]   new_add,
    input                       rst_pc,
    input                       btb_hit,
    input   [ADD_WIDTH-1 : 0]   btb_pc,

    input                       cfr_sig,
    output                      req_sig,
    output  [ADD_WIDTH-1 : 0]   add,
    output  [ADD_WIDTH-1 : 0]   old_pc
);

reg     [ADD_WIDTH-1 : 0]   d_pc, q_pc;
reg                         d_req_sig;

assign  add = (rst_pc)? new_add : q_pc;
assign  req_sig = d_req_sig;
assign  old_pc = q_pc;

always@*
begin
    if (rst)
    begin
        d_pc = 'b0;
        d_req_sig = 'b0;
    end
    else
    begin
        d_pc = add;
        d_req_sig = 'b0;
        if (en)
        begin
            d_req_sig = 1'b1;
            if (cfr_sig)
                if (btb_hit)
                    d_pc = btb_pc;
                else
                    d_pc = add + 8;
        end
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_pc <= 'b0;
    end
    else
    begin
        q_pc <= d_pc;
    end
end
endmodule