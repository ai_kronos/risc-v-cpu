`define btb_line 5
`define bht_line 5
module bht (
    input                                       clk,
    input                                       rst,
    input           [17 : 0]                    add,
    input                                       taken,
    input                                       w_en,
    output                                      pre_taken
);
reg     [`bht_line-1 : 0]       q_his;
wire    [`bht_line-1 : 0]       d_his;

reg     [1 : 0]                 q_bht [2**`bht_line-1 : 0];
reg     [1 : 0]                 d_bht [2**`bht_line-1 : 0];

wire    [`bht_line-1 : 0]       line_num;
assign line_num = q_his ^ add[`bht_line+1 : 2];
wire    [`bht_line-1 : 0]       n_line_num;
assign n_line_num = d_his ^ add[`bht_line+1 : 2];

assign d_his = (w_en)? ((q_his << 1) | taken) : q_his;
assign pre_taken = q_bht[n_line_num][1];

integer i;

always@*
begin
    for (i = 0; i < 2**`bht_line; i = i + 1)
        d_bht[i] = q_bht[i];

    if (w_en)
    begin
        if (taken)
        begin
            if (~(&d_bht[line_num]))
                d_bht[line_num] = q_bht[line_num] + 1;
        end
        else
        begin
            if (|d_bht[line_num])
                d_bht[line_num] = q_bht[line_num] - 1;
        end
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_his <= 'b0;
        for (i = 0; i < 2**`bht_line; i = i + 1)
            q_bht[i] <= 2'b10;
    end
    else
    begin
        q_his <= d_his;
        for (i = 0; i < 2**`bht_line; i = i + 1)
            q_bht[i] <= d_bht[i];
    end
end
endmodule

module btb (
    input   wire                                clk,
    input   wire                                rst,
    input   wire    [17 : 0]                    add,
    input   wire    [17 : 0]                    add_w,
    input   wire                                en_w,
    input   wire    [18 : 0]                    data_w,
    output  wire    [17 : 0]                    data,
    output  wire                                hit,

    output  wire    [17 : 0]                    try_data
);
//stock
reg     [17-2-`btb_line : 0]        hi_add  [2**`btb_line - 1 : 0];
reg     [18 : 0]                    dat     [2**`btb_line - 1 : 0];

//logic
wire    [`btb_line-1 : 0]           line_num;
wire    [17-2-`btb_line : 0]        d_hi_add;
wire    [`btb_line-1 : 0]           line_num_w;
wire    [17-2-`btb_line : 0]        d_hi_add_w;

assign  line_num        = add[`btb_line+1 : 2];
assign  d_hi_add        = add[17 : `btb_line+2];
assign  line_num_w      = add_w[`btb_line+1 : 2];
assign  d_hi_add_w      = add_w[17 : `btb_line+2];

assign  data            = dat[line_num][17 : 0];
assign  try_data        = dat[line_num_w][17 : 0];
assign  hit = (dat[line_num][18] & (hi_add[line_num] == d_hi_add));

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 2**`btb_line; i = i + 1)
        begin
            dat[i]      <=  'b0;
            hi_add[i]   <=  'b0;
        end
    end
    else if (en_w)
        begin
            dat[line_num_w]     <= data_w;
            hi_add[line_num_w]  <= d_hi_add_w;
//            $display("%h %h %b\n", add_w, data_w[17 : 0], data_w[18]);
        end
end
endmodule

module pc_buffer (
    input               clk,
    input               rst,
    input               w_en,
    input               r_en,
    input   [18 : 0]    w_data,
    output  [18 : 0]    r_data
);

reg     [2 : 0]         q_head, q_tail;
reg     [18 : 0]        q_buff [7 : 0];

wire    [2 : 0]         d_head, d_tail;

assign d_head = (r_en)? q_head + 1 : q_head;
assign d_tail = (w_en)? q_tail + 1 : q_tail;

assign r_data = q_buff[q_head];

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        q_head      <= 'b0;
        q_tail      <= 'b0;
        for (i = 0; i < 8; i = i + 1)
            q_buff[i] = 'b0;
    end
    else
    begin
        q_head      <= d_head;
        q_tail      <= d_tail;
        if (w_en)
            q_buff[q_tail] = w_data;
    end
end
endmodule