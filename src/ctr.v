module ctr (
    input               clk,
    input               rst,

    input               dp_suc,

    input               is_branch0,
    input               is_branch1,
    input               ins2_invalid,
    input               branch_en,
    input   [17 : 0]    in_pc,
    input   [17 : 0]    pc2,
    input               btb_hit,
    input               req_sig,
    input               cfr_sig,
    input               pre_fal,

//debug
    input               ins1_valid,
    input               ins2_valid,

    input               dp1_suc,
    input               dp2_suc,
//debug

    output              if_en,
    output              rst_pc,
    output  [17 : 0]    new_pc,

    output  [18 : 0]    pc_buff_w_data,
    output              dp_en
);
reg                 d_status, q_status;
reg     [17 : 0]    d_in_pc, q_in_pc;
wire    [17 : 0]    pc1;
assign pc1 = q_in_pc;

assign if_en = dp_suc;
assign rst_pc = ((is_branch0 | is_branch1 | ins2_invalid) & ~q_status) | (branch_en & pre_fal);

reg     [31 : 0]    stop;

reg     [17 : 0]    d_new_pc;
assign new_pc = d_new_pc;

//debug
assign dp_en = 1'b1;
//assign dp_en = (q_cnt < stop)? 1'b1 : 'b0;
reg     [31 : 0]    d_cnt, q_cnt;
always@*
begin
//    stop = 1000;
    if (rst)
    begin
        d_cnt = 'b0;
    end
    else
    begin
        d_cnt = q_cnt + dp1_suc + dp2_suc + branch_en;
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_cnt           <= 'b0;
    end
    else
    begin
        q_cnt           <= d_cnt;
    end
end
//debug

assign pc_buff_w_data = {q_status, pc1};

always@*
begin
    d_new_pc = 'b0;
    if (branch_en & pre_fal)
        d_new_pc = pc2;
    else
    begin
        d_new_pc = pc1 + 4;
    end
end

always@*
begin
    d_status = q_status;
    d_in_pc = q_in_pc;
    if (req_sig & cfr_sig)
        d_in_pc = in_pc;
    if (req_sig & cfr_sig & btb_hit)
        d_status = 1'b1;
    else if (ins1_valid | (branch_en & pre_fal))
        d_status = 1'b0;
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_status <= 1'b0;
        q_in_pc  <= 'b0;
    end
    else
    begin
        q_status <= d_status;
        q_in_pc  <= d_in_pc;
    end
end
endmodule