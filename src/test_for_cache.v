`timescale  1ns/1ps

module testbench_top();
parameter   CLK = 2.5;
parameter   CLK_hci = 5;
parameter   RST = 111;

reg         clk, clk_hci, rst;
wire [6:0]     ins_addr, dat_addr,
                mem_add;
wire            ins_sig, dat_sig, dat_w_sig,
                idata_valid,
                ddata_valid,
                icfr,
                dcfr,
                ram_en,
                mem_wr;
wire [31:0]     idata_out,
                ddata_out;
wire [7:0]      mem_w_data,
                mem_data;
wire [31:0]     dwr_data;
wire                c_in_rdy, cd_in_rdy,
                    c_out_rdy, cd_out_rdy;
reg                 c_en, cd_en, cd_en_w;
reg     [6:0]      c_add, cd_add;
wire    [31:0]       c_data, cd_data;
reg     [31:0]       cd_data_w;
initial
begin
    clk = 1'b1;
    forever
    begin
        #CLK
        begin
            clk = ~clk;
        end
    end
end
initial
begin
    cd_en = 1'b1;
    c_en = 1'b0;
    #100000
    c_en = 1'b1;
    cd_en = 1'b0;
end
always@(posedge clk)
begin
    c_add <= $random();
    cd_add <= $random();
//    c_en <= $random();
//    cd_en <= $random();
    cd_en_w <= $random();
    cd_data_w <= $random();
//    c_add <= 'b0;
//    cd_add <= 'b0;
//    cd_en_w <= 'b0;
//    cd_data_w <= 'b0;
end
initial
begin
    clk_hci = 1'b0;
    #1;
    forever
    begin
        #CLK_hci    clk_hci = ~clk_hci;
    end
end

initial
begin
    rst = 1'b1;
    #RST rst = 1'b0;
end

initial
begin
end

assign 		ram_en = (mem_add[16:15] == 2'b11) ? 1'b0 : 1'b1;

mem_ctr #(.ADD_WIDTH(7)) mem0 (
    .clk(clk),
    .clk_mem(clk_hci),
    .rst(rst),
    .iadd(ins_addr),
    .dadd(dat_addr),
    .ireq(ins_sig),
//    .dreq(dat_sig),
    .dreq(cd_en),
//    .dwrt(dat_w_sig),
    .dwrt(cd_en_w),
//    .dwr_data(dwr_data),
    .dwr_data(cd_data_w),
    .idata_out(idata_out),//o
    .ddata_out(ddata_out),//o
    .idata_valid(idata_valid),//o
    .ddata_valid(ddata_valid),//o
    .icfr(icfr),//o
    .dcfr(dcfr),//o

    .mem_data(mem_data),
    .mem_wr(mem_wr),//o
    .mem_add(mem_add),//o
    .mem_w_data(mem_w_data)//o
);
reg     [31 : 0]     d_std_mem_idata;
reg     [31 : 0]     d_std_mem_ddata;
reg     [31:0]      ram_data [31 : 0];
integer i;
always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 32; i = i + 1) begin
            ram_data[i] <= 'b0;
        end
    end
    else
    begin
            if (dcfr && cd_en)
            begin
                if (cd_en_w)
                    ram_data[cd_add[6:2]] <= cd_data_w;
                d_std_mem_ddata <= ram_data[cd_add[6:2]];
            end
            if (c_in_rdy && c_en)
            begin
                d_std_mem_idata <= ram_data[c_add[6:2]];
            end
            /*
            if (c_in_rdy && c_en)
            begin
                d_std_mem_idata = ram_data[c_add[5:2]];
            end
            */
    end
end
ram #(.ADDR_WIDTH(7)) ram0(
	.clk_in(clk_hci),
	.en_in(1'b1),
	.r_nw_in(~mem_wr),
	.a_in(mem_add),
	.d_in(mem_w_data),
	.d_out(mem_data)
);
ins_cache #(.LINE(2), .ADD_WIDTH(7)) ca0(
    .clk(clk),
    .rst(rst),
    .in_rdy(c_in_rdy),//o
    .add(c_add),
    .en(c_en),
    .data(c_data),//o
    .out_rdy(c_out_rdy),//o

    .t_mem_rdy(icfr),
    .mem_data(idata_out),
    .mem_dat_valid(idata_valid),
    .addr(ins_addr),//o
    .req_sig(ins_sig),//o
    .dat_req_sig(dat_sig)
);
dat_cache #(.LINE(2), .ADD_WIDTH(7)) ca1(
    .clk(clk),
    .rst(rst),
    .in_rdy(cd_in_rdy),
    .add(cd_add),
    .en('b0),
    .en_w(cd_en_w),
    .data_w(cd_data_w),
    .data(cd_data),
    .out_rdy(cd_out_rdy),
    .mem_rdy(dcfr),
    .mem_data(ddata_out),
    .mem_dat_valid(ddata_valid),
    .addr(dat_addr),
    .req_sig(dat_sig),
    .mem_w_req(dat_w_sig),
    .mem_w_data(dwr_data)
);
wire diff, diff2;
assign diff = (d_std_mem_ddata != cd_data && cd_out_rdy)? 1'b1 : 1'b0;
assign diff2 = (d_std_mem_idata != c_data && c_out_rdy)? 1'b1 : 1'b0;
endmodule