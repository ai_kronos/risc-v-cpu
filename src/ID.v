`include "recode.v"
module ins_decode #(
    parameter ADD_WIDTH = 18,
    parameter INS_WIDTH = 32
) (
    input                       clk,
    input                       rst,
    input                       en,
    input   [INS_WIDTH-1 : 0]   ins1,
    input   [INS_WIDTH-1 : 0]   ins2,
    input                       ins1_valid,
    input                       ins2_valid,
    input   [17 : 0]            in_pc,

    output                      de_ins1_valid,
    output                      de_ins2_valid,
    output  [1 : 0]             is_branch,
    output  [`de_width-1 : 0]   de_ins1,
    output  [`de_width-1 : 0]   de_ins2,
    output                      reg_branch_en,

    output  [4 : 0]             rs1_id,
    output  [4 : 0]             rs2_id
);
wire [`de_width-1 : 0]  t_de_ins1, t_de_ins2;

wire                    nins1_valid, nins2_valid;
wire [INS_WIDTH-1 : 0]  nins1, nins2;

reg                     d_ins1_valid_buf, q_ins1_valid_buf,
                        d_ins2_valid_buf, q_ins2_valid_buf;
reg [`de_width-1 : 0]   d_ins1_buf, q_ins1_buf,
                        d_ins2_buf, q_ins2_buf;

assign nins1 = (q_ins1_valid_buf)? q_ins1_buf : ins1;
assign nins2 = (q_ins2_valid_buf)? q_ins2_buf : ins2;
assign nins1_valid = q_ins1_valid_buf | ins1_valid;
assign nins2_valid = q_ins2_valid_buf | ins2_valid;

assign is_branch[0] = nins1[6] & nins1_valid;
assign is_branch[1] = nins2[6] & nins2_valid;

format_decoder decoder1 (
    .ins(nins1),
    .in_pc(in_pc - 8),
    .de_ins(t_de_ins1)
);

format_decoder decoder2 (
    .ins(nins2),
    .in_pc(in_pc - 4),
    .de_ins(t_de_ins2)
);

reg                     d_de_ins1_valid, q_de_ins1_valid,
                        d_de_ins2_valid, q_de_ins2_valid;
reg [`de_width-1 : 0]   d_de_ins1, q_de_ins1,
                        d_de_ins2, q_de_ins2;

assign reg_branch_en = d_de_ins1_valid & d_de_ins1[0];

assign rs1_id = d_de_ins1[`rs1_ind_de +: 5];
assign rs2_id = d_de_ins1[`rs2_ind_de +: 5];

assign de_ins1 = q_de_ins1;
assign de_ins2 = q_de_ins2;
assign de_ins1_valid = q_de_ins1_valid;
assign de_ins2_valid = q_de_ins2_valid;

always@*
begin
    if (rst)
    begin
        d_de_ins1 = 'b0;
        d_de_ins2 = 'b0;
        d_de_ins1_valid = 'b0;
        d_de_ins2_valid = 'b0;

        d_ins1_valid_buf = 'b0;
        d_ins1_buf = 'b0;
        d_ins2_valid_buf = 'b0;
        d_ins2_buf = 'b0;
    end
    else
    begin
        d_de_ins1 = q_de_ins1;
        d_de_ins2 = q_de_ins2;
        d_de_ins1_valid = q_de_ins1_valid;
        d_de_ins2_valid = q_de_ins2_valid;

        d_ins1_valid_buf = q_ins1_valid_buf;
        d_ins1_buf = q_ins1_buf;
        d_ins2_valid_buf = q_ins2_valid_buf;
        d_ins2_buf = q_ins2_buf;


        if (en)
        begin
            d_de_ins1 = t_de_ins1;
            d_de_ins2 = t_de_ins2;
            d_de_ins1_valid = nins1_valid;
            d_de_ins2_valid = nins2_valid & (~is_branch[0]) & (~is_branch[1]);
            
            d_ins1_valid_buf = 'b0;
            d_ins2_valid_buf = 'b0;
        end
        else
        begin
            if (ins1_valid)
            begin
                d_ins1_valid_buf = ins1_valid;
                d_ins2_valid_buf = ins2_valid;
                d_ins1_buf = ins1;
                d_ins2_buf = ins2;
            end
        end
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_de_ins1 <= 'b0;
        q_de_ins2 <= 'b0;
        q_de_ins1_valid <= 'b0;
        q_de_ins2_valid <= 'b0;

        q_ins1_buf <= 'b0;
        q_ins1_valid_buf <= 'b0;
        q_ins2_buf <= 'b0;
        q_ins2_valid_buf <= 'b0;
    end
    else
    begin
        q_de_ins1 <= d_de_ins1;
        q_de_ins2 <= d_de_ins2;
        q_de_ins1_valid <= d_de_ins1_valid;
        q_de_ins2_valid <= d_de_ins2_valid;

        q_ins1_buf <= d_ins1_buf;
        q_ins1_valid_buf <= d_ins1_valid_buf;
        q_ins2_buf <= d_ins2_buf;
        q_ins2_valid_buf <= d_ins2_valid_buf;
    end
end
endmodule

/*
imm:
[11:0]  I_imm:   [11:0]

[11:0]  S_imm:   [11:5][4:0]
[11:0]  B_imm:   [12|10:5][4:1|11]

[31:0]  U_imm:   [31:12] filling in the lowest 12 bits with zeros
[20:0]  J_imm:   [20|10:1|11|19:12] sign-extended
*/
module format_decoder #(
    parameter INS_WIDTH = 32
)(
    input   [INS_WIDTH-1 : 0]       ins,
    input   [17 : 0]                in_pc,
    output  [`de_width-1 : 0]       de_ins
);
wire    [31 : 0]    I_imm, S_imm,
                    B_imm, U_imm,
                    J_imm;

wire    [31 : 0]    r1, r2;
wire    [4 : 0]     rd;
wire    [2 : 0]     funct3;
wire    [6 : 0]     funct7;

reg     [2 : 0]     d_opcode;
reg     [31 : 0]    d_d1, d_d2, d_d3;
reg     [4 : 0]     d_des;
reg     [1 : 0]     d_ready;
reg     [3 : 0]     d_funct;
reg     [7 : 0]     d_funct2;

assign  I_imm = {{20{{ins[31]}}}, ins[31 : 20]};
assign  S_imm = {{21{ins[31]}}, ins[31 : 25], ins[11 : 7]};
assign  B_imm = {{20{ins[31]}}, ins[7], ins[30 : 25], ins[11 : 8], 1'b0};
assign  U_imm = {ins[31 : 12], 12'h000};
assign  J_imm = {{12{ins[31]}}, ins[19 : 12], ins[20], ins[30 : 21], 1'b0};

assign  rd = ins[11 : 7];
assign  r1 = {{27{1'b0}}, ins[19 : 15]};
assign  r2 = {{27{1'b0}}, ins[24 : 20]};

assign  funct3 = ins[14 : 12];
assign  funct7 = ins[31 : 25];

//2,32,32,32,5,8,4,3
assign de_ins = {d_ready, d_d3, d_d2, d_d1, d_des, d_funct2, d_funct, d_opcode};

always@*
begin
    d_d3 = 'b0;
    d_d2 = 'b0;
    d_d1 = 'b0;
    d_funct = 'b0;
    d_funct2 = 'b0;
    case (ins[6 : 0])
        7'b0110111://LUI
        begin
            d_opcode = `main_cal_code;
            d_d2 = U_imm;
            d_d1 = 'b0;
            d_des = rd;
            d_ready = 2'h3;
            d_funct2 = `add_unit;
        end
        7'b0010111://AUIPC
        begin
            d_opcode = `main_cal_code;
            d_d2 = U_imm;
            d_d1 = in_pc;
            d_des = rd;
            d_ready = 2'h3;
            d_funct2 = `add_unit;
        end
        7'b1101111://JAL
        begin
            d_opcode = `branch_code;
            d_d3 = J_imm;
            d_des = rd;
            d_ready = 2'h3;
            d_funct = 4'b1000;
        end
        7'b1100111://JALR
        begin
            d_opcode = `branch_code;
            d_d3 = I_imm;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_d1 = r1;
            d_des = rd;
            d_ready[1] = 1'b1;
            d_funct = 4'b1001;
        end
        7'b1100011://standard branch
        begin
            d_opcode = `branch_code;
            d_d3 = B_imm;
            d_d2 = r2;
            d_d1 = r1;
            d_des = 'b0;
            if (|r2)
                d_ready[1] = 1'b0;
            else
                d_ready[1] = 1'b1;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_funct = {1'b0, funct3};
        end
        7'b0000011://load
        begin
            d_opcode = `l_s_code;
            d_d3 = I_imm;
            d_d1 = r1;
            d_des = rd;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_ready[1] = 1'b1;
            d_funct = {1'b0, funct3};
        end
        7'b0100011://store
        begin
            d_opcode = `l_s_code;
            d_d3 = S_imm;
            d_d2 = r2;
            d_d1 = r1;
            d_des = 'b0;
            if (|r2)
                d_ready[1] = 1'b0;
            else
                d_ready[1] = 1'b1;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_funct = {1'b1, funct3};
        end
        7'b0010011://imm_cal 
        begin
            d_opcode = `main_cal_code;
            d_d2 = I_imm;
            d_d1 = r1;
            d_des = rd;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_ready[1] = 1'b1;
            case (funct3)
                3'b000:
                begin
                    d_funct2 = `add_unit;
                    d_funct = {1'b0, funct3};
                end
                3'b100:
                begin
                    d_funct2 = `xor_unit;
                    d_funct = {1'b0, funct3};
                end
                3'b110:
                begin
                    d_funct2 = `or_unit;
                    d_funct = {1'b0, funct3};
                end
                3'b111:
                begin
                    d_funct2 = `and_unit;
                    d_funct = {1'b0, funct3};
                end
                3'b001:
                begin
                    d_funct2 = `sll_unit;
                    d_funct = {funct7[5], funct3};
                end
                3'b101:
                begin
                    d_funct2 = `srl_unit;
                    d_funct = {funct7[5], funct3};
                end
                3'b010:
                begin
                    d_funct2 = `slt_unit;
                    d_funct = {1'b0, funct3};
                end
                default:
                begin
                    d_funct2 = `sltu_unit;
                    d_funct = {funct7[5], funct3};
                end
            endcase
        end
        default://int_cal 
        begin
            d_opcode = `main_cal_code;
            d_d2 = r2;
            d_d1 = r1;
            d_des = rd;
            if (|r2)
                d_ready[1] = 1'b0;
            else
                d_ready[1] = 1'b1;
            if (|r1)
                d_ready[0] = 1'b0;
            else
                d_ready[0] = 1'b1;
            d_funct = {funct7[5], funct3};
            case (funct3)
                3'b000:
                    d_funct2 = `add_unit;
                3'b100:
                    d_funct2 = `xor_unit;
                3'b110:
                    d_funct2 = `or_unit;
                3'b111:
                    d_funct2 = `and_unit;
                3'b001:
                    d_funct2 = `sll_unit;
                3'b101:
                    d_funct2 = `srl_unit;
                3'b010:
                    d_funct2 = `slt_unit;
                default:
                    d_funct2 = `sltu_unit;
            endcase
        end
    endcase
end
endmodule

module reg_recoder (
    input   [4 : 0]     reg_id,
    output  [31 : 0]    reg_en
);
reg [31 : 0]        d_reg_en;
assign reg_en = d_reg_en;
always@*
begin
    case (reg_id)
        5'd00:
            d_reg_en = `reg_0;
        5'd01:
            d_reg_en = `reg_1;
        5'd02:
            d_reg_en = `reg_2;
        5'd03:
            d_reg_en = `reg_3;
        5'd04:
            d_reg_en = `reg_4;
        5'd05:
            d_reg_en = `reg_5;
        5'd06:
            d_reg_en = `reg_6;
        5'd07:
            d_reg_en = `reg_7;
        5'd08:
            d_reg_en = `reg_8;
        5'd09:
            d_reg_en = `reg_9;
        5'd10:
            d_reg_en = `reg_10;
        5'd11:
            d_reg_en = `reg_11;
        5'd12:
            d_reg_en = `reg_12;
        5'd13:
            d_reg_en = `reg_13;
        5'd14:
            d_reg_en = `reg_14;
        5'd15:
            d_reg_en = `reg_15;
        5'd16:
            d_reg_en = `reg_16;
        5'd17:
            d_reg_en = `reg_17;
        5'd18:
            d_reg_en = `reg_18;
        5'd19:
            d_reg_en = `reg_19;
        5'd20:
            d_reg_en = `reg_20;
        5'd21:
            d_reg_en = `reg_21;
        5'd22:
            d_reg_en = `reg_22;
        5'd23:
            d_reg_en = `reg_23;
        5'd24:
            d_reg_en = `reg_24;
        5'd25:
            d_reg_en = `reg_26;
        5'd27:
            d_reg_en = `reg_27;
        5'd28:
            d_reg_en = `reg_28;
        5'd29:
            d_reg_en = `reg_29;
        5'd30:
            d_reg_en = `reg_30;
        5'd31:
            d_reg_en = `reg_31;
    endcase
end
endmodule