`include "recode.v"

module branch_alu (
    input                       clk,
    input                       rst,

    input                       t_en,
    input   [`rs_width-1 : 0]   t_ins,
    input   [4 : 0]             t_rs_id,
    input                       pre_taken,
    input   [17 : 0]            in_pc,

    input   [31 : 0]            t_rs1,
    input   [31 : 0]            t_rs2,
    input                       t_rs1_suc,
    input                       t_rs2_suc,

    output                      cdb_en,
    output  [4 : 0]             cdb_id,
    output  [31 : 0]            cdb_data,
    output  [4 : 0]             cdb_reg_id,

    output                      branch_en,
    output                      pre_fal,
    output  [31 : 0]            new_pc,
    output                      rdy,
    output  [18 : 0]            w_data,

    input                       cdb2_en,
    input   [4 : 0]             cdb2_id,
    input   [31 : 0]            cdb2_data,
    input                       cdb3_en,
    input   [4 : 0]             cdb3_id,
    input   [31 : 0]            cdb3_data,

    input   [17 : 0]            btb_pc,
    
    output                      p_rdy,
    output                      p_branch_en
);

wire    [31 : 0]        tmp_sub;
assign tmp_sub = rs1 - rs2;

reg     [31 : 0]        d_new_pc;

reg                     taken;

wire                    d_occu, c1_occu;
reg                     q_occu;


wire    [`rs_width-1 : 0]   ins;
wire    [4 : 0]             rs_id;
wire                        en;


assign rdy = ~q_occu;
assign p_rdy = ~d_occu;

reg     [`rs_width-1 : 0]   q_ins;
reg     [4 : 0]             q_rs_id;

wire    [31 : 0]        rs1, rs2;
wire                    rs1_suc, rs2_suc;

assign rs1_suc = (t_rs1_suc) ? 1'b1 : ((cdb2_en & (cdb2_id == t_rs1[4 : 0])) | (cdb3_en & (cdb3_id == t_rs1[4 : 0])));
assign rs2_suc = (t_rs2_suc) ? 1'b1 : ((cdb2_en & (cdb2_id == t_rs2[4 : 0])) | (cdb3_en & (cdb3_id == t_rs2[4 : 0])));
assign rs1 = (t_rs1_suc)? t_rs1 : ((cdb2_en & (cdb2_id == t_rs1[4 : 0]))? cdb2_data : cdb3_data);
assign rs2 = (t_rs2_suc)? t_rs2 : ((cdb2_en & (cdb2_id == t_rs2[4 : 0]))? cdb2_data : cdb3_data);
/*
assign rs1_suc = t_rs1_suc;
assign rs2_suc = t_rs2_suc;
assign rs1 = t_rs1;
assign rs2 = t_rs2;
*/

assign c1_occu = (ins[`rs_width-1] | rs2_suc) & (ins[`rs_width-2] | rs1_suc);
assign d_occu = (c1_occu)? 1'b0 : (q_occu | en);

wire    [17 : 0]        taken_pc;
reg     [17 : 0]        q_taken_pc;

always@(posedge clk)
begin
    if (rst)
    begin
        q_occu <= 'b0;
    end
    else
    begin
        q_occu <= d_occu;
    end
end

always@*
begin
    case (ins[`funct_rs +: 4])
        4'b0000:
        begin
            if (rs1 == rs2)
                taken = 1'b1;
            else
                taken = 1'b0;
        end
        4'b0001:
        begin
            if (rs1 != rs2)
                taken = 1'b1;
            else
                taken = 1'b0;
        end
        4'b0100:
        begin
            if (tmp_sub[31])
                taken = 1'b1;
            else
                taken = 1'b0;
        end
        4'b0101:
        begin
            if (tmp_sub[31])
                taken = 1'b0;
            else
                taken = 1'b1;
        end
        4'b0110:
        begin
            if (rs1 < rs2)
                taken = 1'b1;
            else
                taken = 1'b0;
        end
        4'b0111:
        begin
            if (rs1 >= rs2)
                taken = 1'b1;
            else
                taken = 1'b0;
        end
        default:
            taken = 1'b1;
    endcase
end

assign taken_pc = (ins[0] & ins[3])? (rs1 + ins[`rs3_ind_rs +: 32]) : (in_pc + ins[`rs3_ind_rs +: 32]);

always@*
begin
    d_new_pc = in_pc + 32'h00000004;
    if (taken)
        if (ins[0] & ins[3])
            d_new_pc = rs1 + ins[`rs3_ind_rs +: 32];
        else
            d_new_pc = in_pc + ins[`rs3_ind_rs +: 32];
end

wire                            d_cdb_en, d_branch_en, d_pre_fal;
wire        [4 : 0]             d_cdb_id, d_cdb_reg_id;
wire        [31 : 0]            d_cdb_data;

reg                             q_cdb_en, q_branch_en, q_pre_fal;
reg         [4 : 0]             q_cdb_id, q_cdb_reg_id;
reg         [31 : 0]            q_cdb_data, q_new_pc;
reg                             q_taken_delay;

always@(posedge clk)
begin
    if (rst)
    begin
        q_cdb_en <= 'b0;
        q_branch_en <= 'b0;
        q_cdb_id <= 'b0;
        q_cdb_reg_id <= 'b0;
        q_cdb_data <= 'b0;
        q_new_pc <= 'b0;
        q_pre_fal <= 'b0;
        q_taken_pc <= 'b0;
    end
    else
    begin
        q_cdb_en <= d_cdb_en;
        q_branch_en <= d_branch_en;
        q_cdb_id <= d_cdb_id;
        q_cdb_reg_id <= d_cdb_reg_id;
        q_cdb_data <= d_cdb_data;
        q_new_pc <= d_new_pc;
        q_pre_fal <= d_pre_fal;
        q_taken_pc <= taken_pc;
    end
end

assign cdb_en = q_cdb_en;
assign branch_en = q_branch_en;
assign p_branch_en = d_branch_en;
assign cdb_id = q_cdb_id;
assign cdb_reg_id = q_cdb_reg_id;
assign cdb_data = q_cdb_data;
assign new_pc = q_new_pc;
assign pre_fal = q_pre_fal;

assign d_cdb_en = ((en | q_occu) & c1_occu) & ins[`funct_rs+3];
assign d_branch_en = (en | q_occu) & c1_occu;
assign d_cdb_id = rs_id;
assign d_cdb_reg_id = ins[`rd_ind_rs+4 : `rd_ind_rs];
assign d_cdb_data = in_pc + 4;
//assign d_pre_fal = (pre_taken ^ taken) | (pre_taken & (|(btb_pc ^ d_new_pc[17 : 0])));
assign d_pre_fal = (pre_taken ^ taken) | (ins[0] & ins[3]);
/*
always@*
begin
    if(d_pre_fal & (~(pre_taken ^ taken)) & pre_taken)
    begin
        $display("ins: %b\n", ins[`funct_rs +: 4]);
        $display("btb: %b\n", btb_pc);
        $display("new: %b\n", d_new_pc[17 : 0]);
        $display("pre_taken: %b\n", pre_taken);
        $display("taken: %b\n", taken);
    end
end
*/

assign en = (q_occu)? 1'b1 : t_en;
assign ins = (q_occu)? q_ins : t_ins;
assign rs_id = (q_occu)? q_rs_id : t_rs_id;
assign w_data = {q_taken_delay, q_taken_pc};

always@(posedge clk)
begin
    if (rst)
    begin
        q_rs_id         <= 'b0;
        q_ins           <= 'b0;
        q_taken_delay   <= 'b0;
    end
    else
    begin
        q_rs_id         <= rs_id;
        q_ins           <= ins;
        q_taken_delay   <= taken;
    end
end
endmodule

module main_alu (
    input                       clk,
    input                       rst,

    input                       en,
    input   [`rs_width-1 : 0]   ins,
    input   [4 : 0]             rs_id,

    output                      cdb_en,
    output  [4 : 0]             cdb_id,
    output  [31 : 0]            cdb_data,
    output  [4 : 0]             cdb_reg_id
);
wire    [31 : 0]                tmp_sub;
assign  tmp_sub = ins[`rs1_ind_rs+31 : `rs1_ind_rs] - ins[`rs2_ind_rs+31 : `rs2_ind_rs];

reg     [31 : 0]                d_result;

wire    [31 : 0]                t_val_add,
                                t_val_shl,
                                t_val_shr,
                                t_val_shru,
                                t_val_xor,
                                t_val_or,
                                t_val_and;

assign t_val_add  = ins[`rs1_ind_rs+31 : `rs1_ind_rs] + ins[`rs2_ind_rs+31 : `rs2_ind_rs];
assign t_val_shl  = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] << ins[`rs2_ind_rs+4 : `rs2_ind_rs]);

assign t_val_shr  = ({32{ins[`rs1_ind_rs+31]}} << (32 - ins[`rs2_ind_rs+4 : `rs2_ind_rs]))
                        | (ins[`rs1_ind_rs+31 : `rs1_ind_rs] >> ins[`rs2_ind_rs+4 : `rs2_ind_rs]);
assign t_val_shru = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] >> ins[`rs2_ind_rs+4 : `rs2_ind_rs]);

assign t_val_xor  = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] ^ ins[`rs2_ind_rs+31 : `rs2_ind_rs]);
assign t_val_or   = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] | ins[`rs2_ind_rs+31 : `rs2_ind_rs]);
assign t_val_and  = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] & ins[`rs2_ind_rs+31 : `rs2_ind_rs]);

reg                             q_cdb_en;
reg     [4 : 0]                 q_cdb_id,
                                q_cdb_reg_id;
reg     [31 : 0]                q_cdb_data;

/*
assign cdb_en = q_cdb_en;
assign cdb_id = q_cdb_id;
assign cdb_reg_id = q_cdb_reg_id;
assign cdb_data = q_cdb_data;
*/

assign cdb_en = en;
assign cdb_id = rs_id;
assign cdb_reg_id = ins[`rd_ind_rs+4 : `rd_ind_rs];
assign cdb_data = d_result;

always@(posedge clk)
begin
    if (rst)
    begin
        q_cdb_en <= 'b0;
        q_cdb_id <= 'b0;
        q_cdb_reg_id <= 'b0;
        q_cdb_data <= 'b0;
    end
    else
    begin
        q_cdb_en = en;
        q_cdb_id = rs_id;
        q_cdb_reg_id = ins[`rd_ind_rs+4 : `rd_ind_rs];
        q_cdb_data = d_result;
    end
end


wire    [31 : 0]                t_res_add,
                                t_res_shr,
                                t_res_xor,
                                t_res_or,
                                t_res_and,
                                t_res_un1,
                                t_res_un2;

assign t_res_add = (ins[`funct_rs+3])? tmp_sub : t_val_add;
assign t_res_shr = (ins[`funct_rs+3])? t_val_shr : t_val_shru;
assign t_res_un1 = (tmp_sub[31])? 32'h0001 : 32'h0000;
assign t_res_un2 = (ins[`rs1_ind_rs+31 : `rs1_ind_rs] < ins[`rs2_ind_rs+31 : `rs2_ind_rs])? 32'h0001 : 32'h0000;

always@*
begin
    d_result = 'b0;
    case (1'b1)
        ins[`funct2_rs]:
            d_result = t_res_add;
        ins[`funct2_rs+1]:
            d_result = t_val_shl;
        ins[`funct2_rs+2]:
            d_result = t_res_shr;
        ins[`funct2_rs+3]:
            d_result = t_res_un1;
        ins[`funct2_rs+4]:
            d_result = t_res_un2;
        ins[`funct2_rs+5]:
            d_result = t_val_xor;
        ins[`funct2_rs+6]:
            d_result = t_val_or;
        ins[`funct2_rs+7]:
            d_result = t_val_and;
    endcase
end
endmodule

module l_s_alu #(
    parameter ADD_WIDTH = 18
) (
    input                       clk,
    input                       rst,

    input                       en,
    input   [`rs_width-1 : 0]   ins,
    input   [4 : 0]             rs_id,

    input   [31 : 0]            mem_data,
    input                       data_valid,
    input                       mem_cfr,
    output  [ADD_WIDTH-1 : 0]   mem_add,
    output  [31 : 0]            mem_w_data,
    output  [31 : 0]            mem_w_mask,
    output                      req_sig,
    output                      req_w,

    output                      cdb_en,
    output  [4 : 0]             cdb_id,
    output  [31 : 0]            cdb_data,
    output  [4 : 0]             cdb_reg_id,
    output  [1 : 0]             out_status
);
reg     [1 : 0]             d_status, q_status;//0:idle 1:waiting for free of mem 2:waiting for data 3:waiting for write
reg     [`rs_width-1 : 0]   d_ins, q_ins;
reg     [4 : 0]             d_rs_id, q_rs_id;

reg     [31 : 0]            d_cdb_data;

reg                             d_req_sig;
reg                             d_req_w;

reg     [31 : 0]                d_mem_w_data,
                                d_add, q_add;

wire    [31 : 0]                tmp_add,
                                ex_data,
                                ex_b_data,
                                ex_h_data;

assign out_status = q_status;

sig_ext #(.IN_WIDTH(8)) sig0(.inp(mem_data[{q_add[1 : 0], 3'b0} +: 8]), .out(ex_b_data));
sig_ext #(.IN_WIDTH(16)) sig1(.inp(mem_data[{q_add[1], 4'b0000} +: 8]), .out(ex_h_data));
assign ex_data = (q_ins[`funct_rs])? ex_h_data : ex_b_data;

wire                            l_s_tag;
wire                            sig_ex;


assign req_sig = d_req_sig;
assign req_w = d_req_w;
assign mem_add = q_add[ADD_WIDTH-1 : 0];
//has changed
assign cdb_id = q_rs_id;
assign cdb_reg_id = q_ins[`rd_ind_rs+4 : `rd_ind_rs];
assign cdb_data = d_cdb_data;
assign cdb_en = (q_status == 2'h2 & data_valid)? 1'b1 : 1'b0;

assign tmp_add = d_ins[`rs1_ind_rs+31 : `rs1_ind_rs] + d_ins[`rs3_ind_rs+31 : `rs3_ind_rs];
assign l_s_tag = d_ins[`funct_rs+3];
assign sig_ex = d_ins[`funct_rs+2];

reg     [31 : 0]            d_mask;

always@*
begin
    case (q_ins[`funct_rs +: 2])
        2'h00:
            d_mask = 32'h000000ff;
        2'h01:
            d_mask = 32'h0000ffff;
        default:
            d_mask = 32'hffffffff;
    endcase
end

assign mem_w_data = (d_mem_w_data << {q_add[1 : 0], 3'b0});
assign mem_w_mask = (d_mask << {q_add[1 : 0], 3'b0});

always@*
begin
    case (q_ins[`funct_rs+2 : `funct_rs])
        3'b000, 3'b001:
            d_cdb_data = ex_data;
        3'b010:
            d_cdb_data = mem_data;
        3'b100:
            d_cdb_data = {{24{1'b0}}, mem_data[{q_add[1 : 0], 3'b0} +: 8]};
        3'b101:
            d_cdb_data = {{16{1'b0}}, mem_data[{q_add[1], 4'b0} +: 16]};
        default:
            d_cdb_data = 'b0;
    endcase
end

always@*
begin
    d_req_sig = 'b0;
    d_req_w = 'b0;
    d_mem_w_data = 'b0;
    case (q_status)
        2'h1:
        begin
            d_req_sig = 1'b1;
            d_req_w = 1'b0;
        end
        2'h3:
        begin
            d_mem_w_data = q_ins[`rs2_ind_rs+31 : `rs2_ind_rs];
            d_req_sig = 1'b1;
            d_req_w = 1'b1;
        end
    endcase
end

always@*
begin
    d_ins = q_ins;
    d_status = q_status;
    d_rs_id = q_rs_id;
    d_add = q_add;
    case (q_status)
        2'h0:
        begin
            if (en)
            begin
                d_ins = ins;
                d_add = tmp_add;
                d_rs_id = rs_id;
                if (ins[`funct_rs+3] & (ins[`funct_rs+1] | (tmp_add[17] & tmp_add[16])))
                    d_status = 2'h3;
                else
                    d_status = 2'h1;
            end
        end
        2'h1:
        begin
            if (mem_cfr)
            begin
                if (q_ins[`funct_rs+3])
                    d_status = 2'h3;
                else
                    d_status = 2'h2;
            end
        end
        2'h2:
        begin
            if (data_valid)
            begin
                if (en)
                begin
                    d_ins = ins;
                    d_add = tmp_add;
                    d_rs_id = rs_id;
                    if (ins[`funct_rs+3] & (ins[`funct_rs+1] | (tmp_add[17] & tmp_add[16])))
                        d_status = 2'h3;
                    else
                        d_status = 2'h1;
                end
                else
                    d_status = 2'h0;
            end
        end
        default:
        begin
            if (mem_cfr)
            begin
                if (en)
                begin
                    d_ins = ins;
                    d_add = tmp_add;
                    d_rs_id = rs_id;
                    if (ins[`funct_rs+3] & (ins[`funct_rs+1] | (tmp_add[17] & tmp_add[16])))
                        d_status = 2'h3;
                    else
                        d_status = 2'h1;
                end
                else
                    d_status = 2'h0;
            end
        end
    endcase
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_ins <= 'b0;
        q_status <= 'b0;
        q_rs_id <= 'b0;
        q_add <= 'b0;
    end
    else
    begin
        q_ins <= d_ins;
        q_status <= d_status;
        q_rs_id <= d_rs_id;
        q_add <= d_add;
    end
end
endmodule

module sig_ext #(
    parameter IN_WIDTH = 8,
    parameter OUT_WIDTH = 32
) (
    input   [IN_WIDTH-1 : 0]       inp,
    output  [OUT_WIDTH-1 : 0]   out
);
assign out = ({OUT_WIDTH{inp[IN_WIDTH-1]}} << IN_WIDTH) | inp;
endmodule