`include "recode.v"

module rs_heap_fifo (
    input                       clk,
    input                       rst,

    input                       o_suc,
/*
    input                       cdb1_en,
    input   [4 : 0]             cdb1_id,
    input   [31 : 0]            cdb1_data,
*/
    input                       cdb2_en,
    input   [4 : 0]             cdb2_id,
    input   [31 : 0]            cdb2_data,
    input                       cdb3_en,
    input   [4 : 0]             cdb3_id,
    input   [31 : 0]            cdb3_data,
    input                       w_en1,
    input   [`rs_width-1 : 0]   w_ins1,
    input                       w_en2,
    input   [`rs_width-1 : 0]   w_ins2,

    output                      empty1,
    output                      empty2,
    output  [4 : 0]             rs_id1,
    output  [4 : 0]             rs_id2,
    output  [4 : 0]             rs_id,
    output                      rdy,
    output  [`rs_width-1 : 0]   ex_ins,

    input   [1 : 0]             in_status,
    input                       data_valid,
    input                       mem_cfr,
    output                      suc
);

wire                            cfr;
assign cfr = (|q_cnt) & ((in_status == 2'h0) | (in_status == 2'h2 & data_valid) | (in_status == 2'h3 & mem_cfr));

reg     [2 : 0]                 d_cnt, q_cnt;
reg     [7 : 0]                 d_head, q_head,
                                d_tail, q_tail,
                                d_w_en1, d_w_en2;

wire    [7 : 0]                 t_cfr_sig,
                                t_occu_sig,
                                t_rdy_sig,
                                t_w_en1,
                                t_w_en2,
                                t_en_2;
wire    [`rs_width-1 : 0]       t_ex_ins [7 : 0];
wire    [2 : 0]                 t_rs_id1, t_rs_id2,
                                t_rs_id;
reg     [`rs_width-1 : 0]       d_ex_ins;

assign empty1 = (q_cnt != 3'd7)? 1'b1 : 1'b0;
assign empty2 = (q_cnt < 3'd6)? 1'b1 : 1'b0;
assign rdy = (t_rdy_sig & q_head)? 1'b1 : 1'b0;
assign rs_id1 = {2'b00, t_rs_id1};
assign rs_id2 = {2'b00, t_rs_id2};

assign rs_id = {2'b00, t_rs_id};

assign t_cfr_sig = (cfr)? q_head : 'b0;
assign ex_ins = d_ex_ins;
assign t_w_en1 = d_w_en1;
assign t_w_en2 = d_w_en2;
assign t_en_2 = (q_tail[7])? 8'h01 : (q_tail << 1'b1);

en_to_id etd0 (
    .en(q_tail),
    .id(t_rs_id1)
);
en_to_id etd1 (
    .en(t_en_2),
    .id(t_rs_id2)
);
en_to_id etd2 (
    .en(q_head),
    .id(t_rs_id)
);

assign suc = ((w_en1 ^ w_en2) & empty1) | (w_en1 & w_en2 & empty2) | (~w_en1 & ~w_en2);

always@*
begin
    case(1'b1)
        t_cfr_sig[0]:
            d_ex_ins = t_ex_ins[0];
        t_cfr_sig[1]:
            d_ex_ins = t_ex_ins[1];
        t_cfr_sig[2]:
            d_ex_ins = t_ex_ins[2];
        t_cfr_sig[3]:
            d_ex_ins = t_ex_ins[3];
        t_cfr_sig[4]:
            d_ex_ins = t_ex_ins[4];
        t_cfr_sig[5]:
            d_ex_ins = t_ex_ins[5];
        t_cfr_sig[6]:
            d_ex_ins = t_ex_ins[6];
        default:
            d_ex_ins = t_ex_ins[7];
    endcase

    d_w_en1 = 'b0;
    d_w_en2 = 'b0;

    if (w_en1 & suc & o_suc)
    begin
            d_w_en1 = q_tail;
    end

    if (w_en2 & suc & o_suc)
    begin
        if (w_en1)
            d_w_en2 = t_en_2;
        else
            d_w_en2 = q_tail;
    end
end

always@*
begin
    if (rst)
    begin
        d_cnt = 'b0;
        d_head = 8'h01;
        d_tail = 8'h01;
    end
    else
    begin
        d_cnt = q_cnt + (w_en1 & suc & o_suc) + (suc & w_en2 & o_suc) - (cfr & rdy);
        d_head = q_head;
        d_tail = q_tail;

        if (cfr & rdy)
        begin
            if (q_head[7])
                d_head = 8'h01;
            else
                d_head = (q_head << 1'b1);
        end

        if ((w_en1 ^ w_en2) & empty1 & o_suc)
        begin
            if (q_tail[7])
                d_tail = 8'h01;
            else
                d_tail = (q_tail << 1'b1);
        end

        if ((w_en1 & w_en2) & empty2 & o_suc)
        begin
            if (q_tail[7])
                d_tail = 8'h02;
            else if(q_tail[6])
                d_tail = 8'h01;
            else
                d_tail = (q_tail << 3'd2);
        end
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_cnt <= 'b0;
        q_head <= 8'h01;
        q_tail <= 8'h01;
    end
    else
    begin
        q_cnt <= d_cnt;
        q_head <= d_head;
        q_tail <= d_tail;
    end
end

genvar i;
generate
for (i = 0; i < 8; i = i + 1)
begin:fifo_rs_loop
    rs_unit fifo_rs_unit (
        .clk(clk),
        .rst(rst),
        .cfr(t_cfr_sig[i]),
/*
        .cdb1_en(cdb1_en),
        .cdb1_id(cdb1_id),
        .cdb1_data(cdb1_data),
*/
        .cdb2_en(cdb2_en),
        .cdb2_id(cdb2_id),
        .cdb2_data(cdb2_data),
        .cdb3_en(cdb3_en),
        .cdb3_id(cdb3_id),
        .cdb3_data(cdb3_data),
        .w_en1(t_w_en1[i]),
        .w_ins1(w_ins1),
        .w_en2(t_w_en2[i]),
        .w_ins2(w_ins2),

        .occu(t_occu_sig[i]),
        .rdy(t_rdy_sig[i]),
        .ex_ins(t_ex_ins[i])
    );
end
endgenerate
endmodule

module rs_heap_cal (
    input                       clk,
    input                       rst,
    input                       o_suc,
/*
    input                       cdb1_en,
    input   [4 : 0]             cdb1_id,
    input   [31 : 0]            cdb1_data,
*/
    input                       cdb2_en,
    input   [4 : 0]             cdb2_id,
    input   [31 : 0]            cdb2_data,
    input                       cdb3_en,
    input   [4 : 0]             cdb3_id,
    input   [31 : 0]            cdb3_data,
    input                       w_en1,
    input   [`rs_width-1 : 0]   w_ins1,
    input                       w_en2,
    input   [`rs_width-1 : 0]   w_ins2,

    output                      empty1,
    output                      empty2,
    output  [4 : 0]             rs_id1,
    output  [4 : 0]             rs_id2,
    output  [4 : 0]             rs_id,
    output                      rdy,
    output  [`rs_width-1 : 0]   ex_ins,
    output                      suc
);

reg     [`rs_width-1 : 0]   d_ex_ins;

wire    [7 : 0]             t_occu_sig, t_cfr_sig,
                            t_rdy_sig, t_empty_sig,
                            t_w_en1, t_w_en2,
                            t_low1, t_low2,
                            t_rdy_low;
wire    [`rs_width-1 : 0]   t_ex_ins [7 : 0];
wire    [2 : 0]             t_rs_id1, t_rs_id2,
                            t_rs_id;

assign t_w_en1 = (w_en1 & o_suc & suc)? t_low1 : 'b0;
assign t_w_en2 = (w_en2 & o_suc & suc)? ((w_en1)? t_low2 : t_low1) : 'b0;
assign rdy = |t_rdy_sig;
assign t_cfr_sig = t_rdy_low;

assign t_empty_sig = ~t_occu_sig;

assign rs_id1 = {2'b01, t_rs_id1};
assign rs_id2 = {2'b01, t_rs_id2};

assign rs_id = {2'b01, t_rs_id};

en_to_id etd0 (
    .en(t_low1),
    .id(t_rs_id1)
);
en_to_id etd1 (
    .en(t_low2),
    .id(t_rs_id2)
);
en_to_id etd2 (
    .en(t_rdy_low),
    .id(t_rs_id)
);

lowbit low1 (
    .cod(t_empty_sig),
    .en(t_low1)
);
lowbit low2 (
    .cod(t_empty_sig ^ t_low1),
    .en(t_low2)
);
lowbit low3 (
    .cod(t_rdy_sig),
    .en(t_rdy_low)
);

assign empty1 = |t_low1;
assign empty2 = |t_low2;
assign ex_ins = d_ex_ins;

assign suc = ((w_en1 ^ w_en2) & empty1) | (w_en1 & w_en2 & empty2) | (~w_en1 & ~w_en2);


always@*
begin
    case(1'b1)
        t_cfr_sig[0]:
            d_ex_ins = t_ex_ins[0];
        t_cfr_sig[1]:
            d_ex_ins = t_ex_ins[1];
        t_cfr_sig[2]:
            d_ex_ins = t_ex_ins[2];
        t_cfr_sig[3]:
            d_ex_ins = t_ex_ins[3];
        t_cfr_sig[4]:
            d_ex_ins = t_ex_ins[4];
        t_cfr_sig[5]:
            d_ex_ins = t_ex_ins[5];
        t_cfr_sig[6]:
            d_ex_ins = t_ex_ins[6];
        default:
            d_ex_ins = t_ex_ins[7];
    endcase
end

genvar i;
generate
for (i = 0; i < 8; i = i + 1)
begin:main_rs_loop
    rs_unit main_rs_unit (
        .clk(clk),
        .rst(rst),
        .cfr(t_cfr_sig[i]),
/*
        .cdb1_en(cdb1_en),
        .cdb1_id(cdb1_id),
        .cdb1_data(cdb1_data),
*/
        .cdb2_en(cdb2_en),
        .cdb2_id(cdb2_id),
        .cdb2_data(cdb2_data),
        .cdb3_en(cdb3_en),
        .cdb3_id(cdb3_id),
        .cdb3_data(cdb3_data),
        .w_en1(t_w_en1[i]),
        .w_ins1(w_ins1),
        .w_en2(t_w_en2[i]),
        .w_ins2(w_ins2),

        .occu(t_occu_sig[i]),
        .rdy(t_rdy_sig[i]),
        .ex_ins(t_ex_ins[i])
    );
end
endgenerate
endmodule

module lowbit (
    input   [7 : 0]             cod,
    output  [7 : 0]             en
);
assign  en = cod & (cod ^ (cod - 8'h01));
endmodule

module rs_unit (
    input                       clk,
    input                       rst,
    input                       cfr,
/*
    input                       cdb1_en,
    input   [4 : 0]             cdb1_id,
    input   [31 : 0]            cdb1_data,
*/
    input                       cdb2_en,
    input   [4 : 0]             cdb2_id,
    input   [31 : 0]            cdb2_data,
    input                       cdb3_en,
    input   [4 : 0]             cdb3_id,
    input   [31 : 0]            cdb3_data,
    input                       w_en1,
    input   [`rs_width-1 : 0]   w_ins1,
    input                       w_en2,
    input   [`rs_width-1 : 0]   w_ins2,

    output                      occu,
    output                      rdy,
    output  [`rs_width-1 : 0]   ex_ins
);
reg [`rs_width-1 : 0]   d_rs_data, q_rs_data;
reg                     d_occu, q_occu;

assign  ex_ins = q_rs_data;
assign  rdy = q_occu & q_rs_data[`rs_width-1] & q_rs_data[`rs_width-2];
assign  occu = q_occu;

always@*
begin
    if (rst)
    begin
        d_occu = 1'b0;
        d_rs_data = 'b0;
    end
    else
    begin
        d_occu = q_occu;
        d_rs_data = q_rs_data;

        if (cfr & rdy)
        begin
            d_occu = 1'b0;
        end
        
        if (w_en1 & ~q_occu)
        begin
            d_occu = 1'b1;
            d_rs_data = w_ins1;
        end
        if (w_en2 & ~q_occu & ~w_en1)
        begin
            d_occu = 1'b1;
            d_rs_data = w_ins2;
        end

        if (~q_rs_data[`rs_width-1] & q_occu)
        begin
            case (q_rs_data[`rs2_ind_rs+4 : `rs2_ind_rs])
/*
                cdb1_id:
                begin
                    if (cdb1_en)
                    begin
                        d_rs_data[`rs2_ind_rs+31 : `rs2_ind_rs] = cdb1_data;
                        d_rs_data[`rs_width-1] = 1'b1;
                    end
                end
*/
                cdb2_id:
                begin
                    if (cdb2_en)
                    begin
                        d_rs_data[`rs2_ind_rs+31 : `rs2_ind_rs] = cdb2_data;
                        d_rs_data[`rs_width-1] = 1'b1;
                    end
                end
                cdb3_id:
                begin
                    if (cdb3_en)
                    begin
                        d_rs_data[`rs2_ind_rs+31 : `rs2_ind_rs] = cdb3_data;
                        d_rs_data[`rs_width-1] = 1'b1;
                    end
                end
            endcase
        end

        if (~q_rs_data[`rs_width-2] & q_occu)
        begin
            case (q_rs_data[`rs1_ind_rs+4 : `rs1_ind_rs])
/*
                cdb1_id:
                begin
                    if (cdb1_en)
                    begin
                        d_rs_data[`rs1_ind_rs +31 : `rs1_ind_rs] = cdb1_data;
                        d_rs_data[`rs_width-2] = 1'b1;
                    end
                end
*/
                cdb2_id:
                begin
                    if (cdb2_en)
                    begin
                        d_rs_data[`rs1_ind_rs+31 : `rs1_ind_rs] = cdb2_data;
                        d_rs_data[`rs_width-2] = 1'b1;
                    end
                end
                cdb3_id:
                begin
                    if (cdb3_en)
                    begin
                        d_rs_data[`rs1_ind_rs+31 : `rs1_ind_rs] = cdb3_data;
                        d_rs_data[`rs_width-2] = 1'b1;
                    end
                end
            endcase
        end

    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_occu <= 1'b0;
        q_rs_data <= 'b0;
    end
    else
    begin
        q_occu <= d_occu;
        q_rs_data <= d_rs_data;
    end
end
endmodule

module en_to_id (
    input   [7 : 0]     en,
    output  [2 : 0]     id
);
reg     [2 : 0]         d_id;

assign id = d_id;
always@*
begin
    d_id = 'b0;
    case (1'b1)
        en[0]:
            d_id = 3'd0;
        en[1]:
            d_id = 3'd1;
        en[2]:
            d_id = 3'd2;
        en[3]:
            d_id = 3'd3;
        en[4]:
            d_id = 3'd4;
        en[5]:
            d_id = 3'd5;
        en[6]:
            d_id = 3'd6;
        en[7]:
            d_id = 3'd7;
        default: begin end
    endcase
end
endmodule