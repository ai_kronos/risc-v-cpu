`include "recode.v"
`define data_width 32

module dispatch (
    input                           clk,
    input                           rst,
    input                           en,
    input   [`de_width-1 : 0]       ins1,
    input                           ins1_valid,
    input   [`de_width-1 : 0]       ins2,
    input                           ins2_valid,

    input   [31 : 0]                t_reg_data1,
    input   [31 : 0]                t_reg_data2,
    input   [31 : 0]                t_reg_data3,
    input   [31 : 0]                t_reg_data4,
    input                           t_reg_suc1,
    input                           t_reg_suc2,
    input                           t_reg_suc3,
    input                           t_reg_suc4,

    output  [4 : 0]                 reg_id1,
    output  [4 : 0]                 reg_id2,
    output  [4 : 0]                 reg_id3,
    output  [4 : 0]                 reg_id4,

    output  [4 : 0]                 lock_id1,
    output  [4 : 0]                 lock_rs1,
    output  [4 : 0]                 lock_id2,
    output  [4 : 0]                 lock_rs2,

    input                           t_fifo_empty1,
    input                           t_fifo_empty2,
    input   [4 : 0]                 fifo_rs1_id,
    input   [4 : 0]                 fifo_rs2_id,
    input                           t_main_empty1,
    input                           t_main_empty2,
    input   [4 : 0]                 main_rs1_id,
    input   [4 : 0]                 main_rs2_id,
/*
    input   [4 : 0]                 branch_rs1_id,
    input                           branch_empty,
*/
    
    output                          fifo_w_en1,
    output                          fifo_w_en2,
    output                          main_w_en1,
    output                          main_w_en2,
/*
    output                          branch_w_en1,
    output                          branch_w_en2,
*/
    
    output  [`rs_width-1 : 0]       rs_ins1,
    output  [`rs_width-1 : 0]       rs_ins2,

    output                          rdy,

    input                       w_en1,
    input                       w_en2,
    input                       w_en3,
    input   [4 : 0]             w_rs1,
    input   [`data_width-1 : 0] w_data1,
    input   [4 : 0]             w_rs2,
    input   [`data_width-1 : 0] w_data2,
    input   [4 : 0]             w_rs3,
    input   [`data_width-1 : 0] w_data3,
    //debug
    output                      dp1_suc,
    output                      dp2_suc
);

reg     [`de_width-1 : 0]   d_ins1, q_ins1,
                            d_ins2, q_ins2;


reg         [31 : 0]        reg_data1, reg_data2,
                            reg_data3, reg_data4;

wire                        reg_suc1, reg_suc2,
                            reg_suc3, reg_suc4,
                            t_diff1_1, t_diff1_2, t_diff1_3,
                            t_diff2_1, t_diff2_2, t_diff2_3,
                            t_diff3_1, t_diff3_2, t_diff3_3,
                            t_diff4_1, t_diff4_2, t_diff4_3,
                            dep3, dep4;

assign t_diff1_1 = w_en1 & (w_rs1 == d_ins1[`rs1_ind_de +: 5]);
assign t_diff1_2 = w_en2 & (w_rs2 == t_reg_data1[4 : 0] & ~t_reg_suc1);
assign t_diff1_3 = w_en3 & (w_rs3 == t_reg_data1[4 : 0] & ~t_reg_suc1);
assign t_diff2_1 = w_en1 & (w_rs1 == d_ins1[`rs2_ind_de +: 5]);
assign t_diff2_2 = w_en2 & (w_rs2 == t_reg_data2[4 : 0] & ~t_reg_suc2);
assign t_diff2_3 = w_en3 & (w_rs3 == t_reg_data2[4 : 0] & ~t_reg_suc2);

assign dep3 = (reg_id3 == d_ins1[`rd_ind_de+4 : `rd_ind_de]);
assign dep4 = (reg_id4 == d_ins1[`rd_ind_de+4 : `rd_ind_de]);

assign t_diff3_1 = w_en1 & (w_rs1 == d_ins2[`rs1_ind_de +: 5]) & (~dep3);
assign t_diff3_2 = w_en2 & (w_rs2 == t_reg_data3[4 : 0] & ~t_reg_suc3) & (~dep3);
assign t_diff3_3 = w_en3 & (w_rs3 == t_reg_data3[4 : 0] & ~t_reg_suc3) & (~dep3);
assign t_diff4_1 = w_en1 & (w_rs1 == d_ins2[`rs2_ind_de +: 5]) & (~dep4);
assign t_diff4_2 = w_en2 & (w_rs2 == t_reg_data4[4 : 0] & ~t_reg_suc4) & (~dep4);
assign t_diff4_3 = w_en3 & (w_rs3 == t_reg_data4[4 : 0] & ~t_reg_suc4) & (~dep4);

assign reg_suc1 = t_reg_suc1 | t_diff1_1 | t_diff1_2 | t_diff1_3;
assign reg_suc2 = t_reg_suc2 | t_diff2_1 | t_diff2_2 | t_diff2_3;
assign reg_suc3 = (t_reg_suc3 & ~dep3) | t_diff3_1 | t_diff3_2 | t_diff3_3;
assign reg_suc4 = (t_reg_suc4 & ~dep4) | t_diff4_1 | t_diff4_2 | t_diff4_3;

always@*
begin
    reg_data1 = t_reg_data1;
    case (1'b1)
        t_diff1_2:
            reg_data1 = w_data2;
        t_diff1_3:
            reg_data1 = w_data3;
    endcase
    if (t_diff1_1)
        reg_data1 = w_data1;

    reg_data2 = t_reg_data2;
    case (1'b1)
        t_diff2_2:
            reg_data2 = w_data2;
        t_diff2_3:
            reg_data2 = w_data3;
    endcase
    if (t_diff2_1)
        reg_data2 = w_data1;

    if (dep3)
        reg_data3 = lock_rs1;
    else if (t_diff3_1)
        reg_data3 = w_data1;
    else
        reg_data3 = t_reg_data3;

    case (1'b1)
        t_diff3_2:
            reg_data3 = w_data2;
        t_diff3_3:
            reg_data3 = w_data3;
    endcase

    if (dep4)
        reg_data4 = lock_rs1;
    else if (t_diff4_1)
        reg_data4 = w_data1;
    else
        reg_data4 = t_reg_data4;

    case (1'b1)
        t_diff4_2:
            reg_data4 = w_data2;
        t_diff4_3:
            reg_data4 = w_data3;
    endcase
end

reg                         d_rdy, q_rdy,
                            d_ins1_valid, q_ins1_valid,
                            d_ins2_valid, q_ins2_valid;

reg     [4 : 0]             d_lock_id1, d_lock_id2,
                            d_lock_rs1, d_lock_rs2;

reg     [`rs_width-1 : 0]   d_rs_ins1, d_rs_ins2;
wire    [`rs_width-1 : 0]   t_rs_ins1, t_rs_ins2;

wire    [2 : 0]             t_empty;

wire                        fifo_empty1, fifo_empty2,
                            main_empty1, main_empty2;

assign  fifo_empty1 = t_fifo_empty1 & en;
assign  fifo_empty2 = t_fifo_empty2 & en;
assign  main_empty1 = t_main_empty1 & en;
assign  main_empty2 = t_main_empty2 & en;

assign  rdy = q_rdy;
assign  t_empty = {main_empty1, fifo_empty1, 1'b0};

assign  reg_id1 = d_ins1[`rs1_ind_de+4 : `rs1_ind_de];
assign  reg_id2 = d_ins1[`rs2_ind_de+4 : `rs2_ind_de];
assign  reg_id3 = d_ins2[`rs1_ind_de+4 : `rs1_ind_de];
assign  reg_id4 = d_ins2[`rs2_ind_de+4 : `rs2_ind_de];

assign  main_w_en1 = d_ins1[2] & d_ins1_valid & en;
assign  main_w_en2 = d_ins2[2] & d_ins2_valid & en;
assign  fifo_w_en1 = d_ins1[1] & d_ins1_valid & en;
assign  fifo_w_en2 = d_ins2[1] & d_ins2_valid & en;
/*
assign  branch_w_en1 = d_ins1[0] & d_ins1_valid;
assign  branch_w_en2 = d_ins2[0] & d_ins2_valid;
*/

assign  t_rs_ins1[`rs_width-1] = (d_rs_ins1[`rs_width-1])? 1'b1 : reg_suc2;
assign  t_rs_ins1[`rs_width-2] = (d_rs_ins1[`rs_width-2])? 1'b1 : reg_suc1;
assign  t_rs_ins1[`rs2_ind_rs+31 : `rs2_ind_rs] = (~d_rs_ins1[`rs_width-1])? reg_data2 : d_rs_ins1[`rs2_ind_rs+31 : `rs2_ind_rs];
assign  t_rs_ins1[`rs1_ind_rs+31 : `rs1_ind_rs] = (~d_rs_ins1[`rs_width-2])? reg_data1 : d_rs_ins1[`rs1_ind_rs+31 : `rs1_ind_rs];
assign  t_rs_ins1[`rs1_ind_rs-1 : 0] = d_rs_ins1[`rs1_ind_rs-1 : 0];
assign  t_rs_ins1[`rs3_ind_rs +: 32] = d_rs_ins1[`rs3_ind_rs +: 32];

assign  t_rs_ins2[`rs_width-1] = (d_rs_ins2[`rs_width-1])? 1'b1 : reg_suc4;
assign  t_rs_ins2[`rs_width-2] = (d_rs_ins2[`rs_width-2])? 1'b1 : reg_suc3;
assign  t_rs_ins2[`rs2_ind_rs+31 : `rs2_ind_rs] = (~d_rs_ins2[`rs_width-1])? reg_data4 : d_rs_ins2[`rs2_ind_rs+31 : `rs2_ind_rs];
assign  t_rs_ins2[`rs1_ind_rs+31 : `rs1_ind_rs] = (~d_rs_ins2[`rs_width-2])? reg_data3 : d_rs_ins2[`rs1_ind_rs+31 : `rs1_ind_rs];
assign  t_rs_ins2[`rs1_ind_rs-1 : 0] = d_rs_ins2[`rs1_ind_rs-1 : 0];
assign  t_rs_ins2[`rs3_ind_rs +: 32] = d_rs_ins2[`rs3_ind_rs +: 32];

assign  rs_ins1 = t_rs_ins1;
assign  rs_ins2 = t_rs_ins2;

assign  lock_id1 = d_lock_id1;
assign  lock_id2 = d_lock_id2;
assign  lock_rs1 = d_lock_rs1;
assign  lock_rs2 = d_lock_rs2;

always@*
begin
    if (q_rdy)
    begin
        d_ins1 = ins1;
        d_ins1_valid = ins1_valid;
        d_ins2 = ins2;
        d_ins2_valid = ins2_valid;
        d_rs_ins1 = ins1[`de_width-1 : 3];
        d_rs_ins2 = ins2[`de_width-1 : 3];
    end
    else
    begin
        d_ins1_valid = q_ins1_valid;
        d_ins2_valid = q_ins2_valid;
        d_ins1 = q_ins1;
        d_ins2 = q_ins2;
        d_rs_ins1 = q_ins1[`de_width-1 : 3];
        d_rs_ins2 = q_ins2[`de_width-1 : 3];
    end
end

always@*
begin
    d_lock_id1 = 'b0;
    d_lock_id2 = 'b0;
    d_lock_rs1 = 'b0;
    d_lock_rs2 = 'b0;
    d_rdy = q_rdy;
    if (q_rdy)
    begin
        if (ins1_valid & ins2_valid)
        begin
            case (ins1[2 : 1] & ins2[2 : 1])
                2'b10:
                begin
                    if (main_empty2)
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = ins2[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_rs1 = main_rs1_id;
                        d_lock_rs2 = main_rs2_id;
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
                2'b01:
                begin
                    if (fifo_empty2)
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = ins2[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_rs1 = fifo_rs1_id;
                        d_lock_rs2 = fifo_rs2_id;
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
                default:
                begin
                    if ((|(t_empty & ins1[2 : 0])) & (|(t_empty & ins2[2 : 0])))
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = ins2[`rd_ind_de+4 : `rd_ind_de];
                        case (1'b1)
                            ins1[2]:
                            begin
                                d_lock_rs1 = main_rs1_id;
                            end
                            ins1[1]:
                            begin
                                d_lock_rs1 = fifo_rs1_id;
                            end
                            default: begin end
                        endcase

                        case (1'b1)
                            ins2[2]:
                            begin
                                d_lock_rs2 = main_rs1_id;
                            end
                            ins2[1]:
                            begin
                                d_lock_rs2 = fifo_rs1_id;
                            end
                            default: begin end
                        endcase
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
            endcase
        end
        else
        if (ins1_valid)
        begin
            if (|(t_empty & ins1[2 : 0]))
            begin
                d_rdy = 1'b1;
                d_lock_id1 = ins1[`rd_ind_de+4 : `rd_ind_de];
                case (1'b1)
                    ins1[2]:
                    begin
                        d_lock_rs1 = main_rs1_id;
                    end
                    ins1[1]:
                    begin
                        d_lock_rs1 = fifo_rs1_id;
                    end
                    default: begin end
                endcase
            end
            else
                d_rdy = 1'b0;
        end
        else
            d_rdy = 1'b1;
    end
    else
    begin
        if (q_ins1_valid & q_ins2_valid)
        begin
            case (q_ins1[2 : 1] & q_ins2[2 : 1])
                3'b10:
                begin
                    if (main_empty2)
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = q_ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = q_ins2[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_rs1 = main_rs1_id;
                        d_lock_rs2 = main_rs2_id;
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
                3'b01:
                begin
                    if (fifo_empty2)
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = q_ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = q_ins2[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_rs1 = fifo_rs1_id;
                        d_lock_rs2 = fifo_rs2_id;
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
                default:
                begin
                    if ((|(t_empty & q_ins1[2 : 0])) & (|(t_empty & q_ins2[2 : 0])))
                    begin
                        d_rdy = 1'b1;
                        d_lock_id1 = q_ins1[`rd_ind_de+4 : `rd_ind_de];
                        d_lock_id2 = q_ins2[`rd_ind_de+4 : `rd_ind_de];
                        case (1'b1)
                            q_ins1[2]:
                            begin
                                d_lock_rs1 = main_rs1_id;
                            end
                            q_ins1[1]:
                            begin
                                d_lock_rs1 = fifo_rs1_id;
                            end
                            default: begin end
                        endcase

                        case (1'b1)
                            q_ins2[2]:
                            begin
                                d_lock_rs2 = main_rs1_id;
                            end
                            q_ins2[1]:
                            begin
                                d_lock_rs2 = fifo_rs1_id;
                            end
                            default: begin end
                        endcase
                    end
                    else
                    begin
                        d_rdy = 1'b0;
                    end
                end
            endcase
        end
        else
        if (q_ins1_valid)
        begin
            if (|(t_empty & q_ins1[2 : 0]))
            begin
                d_rdy = 1'b1;
                d_lock_id1 = q_ins1[`rd_ind_de+4 : `rd_ind_de];
                case (1'b1)
                    q_ins1[2]:
                    begin
                        d_lock_rs1 = main_rs1_id;
                    end
                    q_ins1[1]:
                    begin
                        d_lock_rs1 = fifo_rs1_id;
                    end
                    default: begin end
                endcase
            end
            else
                d_rdy = 1'b0;
        end
        else
            d_rdy = 1'b1;
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_rdy <= 1'b1;
        q_ins1_valid <= 1'b0;
        q_ins2_valid <= 1'b0;
        q_ins1 <= 'b0;
        q_ins2 <= 'b0;
    end
    else
    begin
        q_rdy <= d_rdy;
        q_ins1_valid <= d_ins1_valid;
        q_ins2_valid <= d_ins2_valid;
        q_ins1 <= d_ins1;
        q_ins2 <= d_ins2;
    end
end


//debug
reg             d_dp1_suc, d_dp2_suc;
assign dp1_suc = d_dp1_suc;
assign dp2_suc = d_dp2_suc;
always@*
begin
    d_dp1_suc = 1'b0;
    d_dp2_suc = 1'b0;
    if (q_rdy)
    begin
        if (ins1_valid & ins2_valid)
        begin
            case (ins1[2 : 1] & ins2[2 : 1])
                2'b10:
                begin
                    if (main_empty2)
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
                2'b01:
                begin
                    if (fifo_empty2)
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
                default:
                begin
                    if ((|(t_empty & ins1[2 : 0])) & (|(t_empty & ins2[2 : 0])))
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
            endcase
        end
        else
        if (ins1_valid)
        begin
            if (|(t_empty & ins1[2 : 0]))
            begin
                d_dp1_suc = 1'b1;
            end
        end
    end
    else
    begin
        if (q_ins1_valid & q_ins2_valid)
        begin
            case (q_ins1[2 : 1] & q_ins2[2 : 1])
                2'b10:
                begin
                    if (main_empty2)
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
                2'b01:
                begin
                    if (fifo_empty2)
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
                default:
                begin
                    if ((|(t_empty & q_ins1[2 : 0])) & (|(t_empty & q_ins2[2 : 0])))
                    begin
                        d_dp1_suc = 1'b1;
                        d_dp2_suc = 1'b1;
                    end
                end
            endcase
        end
        else
        if (q_ins1_valid)
        begin
            if (|(t_empty & q_ins1[2 : 0]))
            begin
                d_dp1_suc = 1'b1;
            end
        end
    end
end
endmodule