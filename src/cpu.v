// RISCV32I CPU top module
// port modification allowed for debugging purposes
`include "recode.v"

module cpu (
    input  wire                 clk_in,			// system clock signal
//    input  wire                 clk_mem,
    input  wire                 rst_in,			// reset signal
	  input  wire					        rdy_in,			// ready signal, pause cpu when low

    input  wire [ 7:0]          t_mem_din,		// data input bus
    output wire [ 7:0]          mem_dout,		// data output bus
    output wire [31:0]          mem_a,			// address bus (only 17:0 is used)
    output wire                 mem_wr,			// write/read signal (1 for write)

	output wire [31:0]			dbgreg_dout		// cpu register output (debugging demo)
);
assign dbgreg_dout = if_add;

wire    [7 : 0]                 mem_din;
reg     [7 : 0]                 q_mem_din;
reg     [31 : 0]                q_mem_a;

always@(posedge clk_in)
begin
  if (rst_in)
  begin
    q_mem_din <= 'b0;
    q_mem_a <= 'b0;
  end
  else
  begin
    q_mem_din <= t_mem_din;
    q_mem_a <= mem_a;
  end
end

assign mem_din = (&q_mem_a[17 : 16])? q_mem_din : t_mem_din;


wire    [1 : 0]                 ls_alu_status;

wire    [`add_width-1 : 0]      if_add, iadd,
                                dadd,
                                old_pc;
wire                            if_req, ireq,
                                dreq, dwrt,
                                ddata_valid,
                                idata_valid,
                                mem_ctr_cfr,
                                ins_cache_cfr,
                                ins1_valid,
                                ins2_valid,
                                t_ins1_valid,
                                t_ins2_valid,
                                de_ins1_valid,
                                de_ins2_valid,
                                dp_rdy,
                                hbyte, hreq,
                                hwrt;

wire                            hci_dat_valid;

wire    [31 : 0]                idata_out,
                                ins1, ins2,
                                dwr_data,
                                ddata_out;

wire [31 : 0] hci_r_data;

wire    [`de_width-1 : 0]       de_ins1, de_ins2;

wire                      ls_rs_rdy;
wire  [4 : 0]             ls_rs_id;
wire  [`rs_width-1 : 0]   ls_rs_ins;

wire  [31 : 0]            ls_mem_data,
                          ls_mem_w_data,
                          ls_mem_w_mask;
//                          dat_cache_data;

wire  [`add_width-1 : 0]  ls_mem_add;

wire                      ls_dat_valid,
                          ls_mem_cfr,
                          ls_req_sig,
                          ls_req_w;

wire                    dat_cache_cfr,
                        dat_cache_en,
                        dat_cache_en_w,
                        dat_cache_out_rdy;

reg                     q_hci_dat_valid_delay;
reg   [31 : 0]          q_hci_r_data_delay;
wire  [31 : 0]          dat_cache_data;

always@(posedge clk_in)
begin
  q_hci_dat_valid_delay <= hci_dat_valid;
  q_hci_r_data_delay <= hci_r_data;
end

assign ls_mem_data = (q_hci_dat_valid_delay)? q_hci_r_data_delay : dat_cache_data;
assign ls_dat_valid = q_hci_dat_valid_delay | dat_cache_out_rdy;
assign ls_mem_cfr = (&ls_mem_add[`add_width-2 +: 2])? mem_ctr_cfr : dat_cache_cfr;
assign dat_cache_en_w = ls_req_w;
assign hwrt = ls_req_w;
assign dat_cache_en = (&ls_mem_add[`add_width-2 +: 2])? 1'b0 : ls_req_sig;
assign hreq = (&ls_mem_add[`add_width-2 +: 2])? ls_req_sig : 1'b0;
assign hbyte = ~((ls_mem_add == 18'h30004) & (~ls_req_w));

mem_ctr mem_ctr(
  .clk(clk_in),
//  .clk_mem(clk_mem),
  .rst(rst_in),
  .iadd(iadd),
  .dadd(dadd),
  .ireq(ireq),
  .dreq(dreq),
  .dwrt(dwrt),
  .dwr_data(dwr_data),
  .idata_out(idata_out),
  .ddata_out(ddata_out),
  .idata_valid(idata_valid),
  .ddata_valid(ddata_valid),
  .mem_ctr_cfr(mem_ctr_cfr),
  
  .hreq(hreq),
  .hbyte(hbyte),
  .hwrt(hwrt),
  .hadd(ls_mem_add),
  .hci_w_data(ls_mem_w_data),
  .hci_r_data(hci_r_data),
  .hci_dat_valid(hci_dat_valid),

  .mem_data(mem_din),
  .mem_wr(mem_wr),
  .mem_add(mem_a),
  .mem_w_data(mem_dout)
);

ins_cache ins_cache (
  .clk(clk_in),
  .rst(rst_in),
  .in_rdy(ins_cache_cfr),
  .add(if_add),
  .en(if_req),
  .data(ins1),
  .try_data(ins2),
  .out_rdy(t_ins1_valid),
  .try_out_rdy(t_ins2_valid),

  .t_mem_rdy(mem_ctr_cfr),
  .mem_data(idata_out),
  .mem_dat_valid(idata_valid),
  .addr(iadd),
  .req_sig(ireq),

  .dat_req_sig(dreq),
  .hci_req_sig(hreq)
);

dat_cache dat_cache (
  .clk(clk_in),
  .rst(rst_in),
  .in_rdy(dat_cache_cfr),
  .add(ls_mem_add),
  .en(dat_cache_en),
  .en_w(dat_cache_en_w),
  .data_w(ls_mem_w_data),
  .mask(ls_mem_w_mask),
  .data(dat_cache_data),
  .out_rdy(dat_cache_out_rdy),

  .t_mem_rdy(mem_ctr_cfr),
  .mem_data(ddata_out),
  .mem_dat_valid(ddata_valid),
  .addr(dadd),
  .req_sig(dreq),
  .mem_w_req(dwrt),
  .mem_w_data(dwr_data),

  .hci_req_sig(hreq)
);

wire              is_branch0,
                  is_branch1,
                  branch_en,
                  if_en,
                  rst_pc;

wire  [31 : 0]    pc2,
                  new_pc;
wire              t_ins2_invalid;

assign t_ins2_invalid = ~ins2_valid & ins1_valid;
assign is_branch0 = ins1_valid & ins1[6];
assign is_branch1 = ins2_valid & ins2[6];
//assign t_ins2_invalid = ins1_valid;

wire              pre_fal, branch_rdy, p_branch_rdy, p_branch_en;

wire  [18 : 0]    btb_w_data;
wire  [17 : 0]    btb_data, try_btb_data;
wire              btb_hit;

wire  [18 : 0]    pc_buff_w_data;
wire  [18 : 0]    pc_buff_r_data;

pc_buffer pc_buffer(
  .clk(clk_in),
  .rst(rst_in | (branch_en & pre_fal)),
  .w_en(is_branch0),
  .r_en(p_branch_en),
  .w_data(pc_buff_w_data),
  .r_data(pc_buff_r_data)
);

reg     [17 : 0]    q_pc_buff_r_delay;

always@(posedge clk_in)
begin
  if (rst_in)
    q_pc_buff_r_delay <= 'b0;
  else
    q_pc_buff_r_delay <= pc_buff_r_data[17 : 0];
end

btb btb(
  .clk(clk_in),
  .rst(rst_in),
  .add(if_add),

  .add_w(q_pc_buff_r_delay),
  .en_w(branch_en & pre_fal),
  .data_w(btb_w_data),

  .data(btb_data),
  .hit(btb_hit),

  .try_data(try_btb_data)
);

//debug
wire          dp_en, dp1_suc, dp2_suc;

ctr ctr(
  .clk(clk_in),
  .rst(rst_in),
  .dp_suc(dp_rdy),
  .is_branch0(is_branch0),
  .is_branch1(is_branch1),
  .branch_en(branch_en),
  .in_pc(if_add),
  .pc2(pc2),
  .btb_hit(btb_hit),
  .req_sig(if_req),
  .cfr_sig(ins_cache_cfr),
  .ins2_invalid(t_ins2_invalid),
  .pre_fal(pre_fal),

  .if_en(if_en),
  .rst_pc(rst_pc),
  .new_pc(new_pc),

  .ins1_valid(ins1_valid),
  .ins2_valid(ins2_valid),

  .pc_buff_w_data(pc_buff_w_data),

  //debug
  .dp_en(dp_en),
  .dp1_suc(dp1_suc),
  .dp2_suc(dp2_suc)
);

ins_fetch ins_fetch (
  .clk(clk_in),
  .rst(rst_in),
  .en((~(de_ins1_valid & de_ins1[0] & ~branch_rdy)) & if_en & rdy_in),
  .new_add(new_pc),
  .rst_pc(rst_pc),
  .btb_hit(btb_hit),
  .btb_pc(btb_data),

  .cfr_sig(ins_cache_cfr),
  .req_sig(if_req),
  .add(if_add),
  .old_pc(old_pc)
);

wire    [4 : 0]       de_rs1_id, de_rs2_id;
wire                        reg_branch_en;

wire                  d_fetch_valid;
reg                   q_fetch_valid;

assign d_fetch_valid = (ins_cache_cfr)? 1'b1 : ((branch_en & pre_fal)? 1'b0 : q_fetch_valid);
assign ins1_valid = q_fetch_valid & t_ins1_valid;
assign ins2_valid = q_fetch_valid & t_ins2_valid;

always@(posedge clk_in)
begin
  if (rst_in)
    q_fetch_valid   <= 1'b1;
  else
    q_fetch_valid   <= d_fetch_valid;
end

ins_decode ins_decode (
  .clk(clk_in),
  .rst(rst_in | (branch_en & pre_fal)),
  .en((~(de_ins1_valid & de_ins1[0] & ~branch_rdy)) & dp_rdy),
  .ins1(ins1),
  .ins2(ins2),
  .ins1_valid(ins1_valid),
  .ins2_valid(ins2_valid),
//  .ins2_valid('b0),
  .in_pc(old_pc),

  .de_ins1_valid(de_ins1_valid),
  .de_ins2_valid(de_ins2_valid),
//  .is_branch({is_branch1, is_branch0}),
  .de_ins1(de_ins1),
  .de_ins2(de_ins2),
  .rs1_id(de_rs1_id),
  .rs2_id(de_rs2_id),
  .reg_branch_en(reg_branch_en)
);

wire    [4 : 0]     reg_id1, reg_id2, reg_id3, reg_id4,
                    lock_id1,
                    lock_id2,
                    lock_rs1,
                    lock_rs2;

wire                reg_suc1, reg_suc2, reg_suc3, reg_suc4;

wire    [31 : 0]    reg_data1,
                    reg_data2,
                    reg_data3,
                    reg_data4,
                    cdb1_data, cdb2_data, cdb3_data;

wire                cdb1_en,
                    cdb2_en,
                    cdb3_en;

wire    [4 : 0]     cdb1_id, cdb1_reg_id,
                    cdb2_id, cdb2_reg_id,
                    cdb3_id, cdb3_reg_id;

wire                        branch_rsuc1;
wire                        branch_rsuc2;
wire    [`data_width-1 : 0] branch_rdata1;
wire    [`data_width-1 : 0] branch_rdata2;


reg_ctr reg_ctr (
  .clk(clk_in),
  .rst(rst_in),

  .r_en(1'b1),
  .r_id1(reg_id1),
  .r_id2(reg_id2),
  .r_id3(reg_id3),
  .r_id4(reg_id4),
  .branch_rid1(de_rs1_id),
  .branch_rid2(de_rs2_id),

  .branch_en(reg_branch_en & p_branch_rdy),

  .r_data1(reg_data1),
  .r_data2(reg_data2),
  .r_data3(reg_data3),
  .r_data4(reg_data4),
  .r_suc1(reg_suc1),
  .r_suc2(reg_suc2),
  .r_suc3(reg_suc3),
  .r_suc4(reg_suc4),
  .lock_id1(lock_id1),
  .lock_rs1(lock_rs1),
  .lock_id2(lock_id2),
  .lock_rs2(lock_rs2),

  .branch_rsuc1(branch_rsuc1),
  .branch_rsuc2(branch_rsuc2),
  .branch_rdata1(branch_rdata1),
  .branch_rdata2(branch_rdata2),

  .w_en1(cdb1_en),
  .w_en2(cdb2_en),
  .w_en3(cdb3_en),
  .w_id1(cdb1_reg_id),
//  .w_rs1(cdb1_id),
  .w_data1(cdb1_data),
  .w_id2(cdb2_reg_id),
  .w_rs2(cdb2_id),
  .w_data2(cdb2_data),
  .w_id3(cdb3_reg_id),
  .w_rs3(cdb3_id),
  .w_data3(cdb3_data)
);

wire                  fifo_empty1,
                      fifo_empty2,
                      fifo_w_en1,
                      fifo_w_en2,
                      fifo_w_sel1,
                      fifo_w_sel2;

wire                  main_empty1,
                      main_empty2,
                      main_w_en1,
                      main_w_en2,
                      main_w_sel1,
                      main_w_sel2,
                      branch_empty,
                      branch_w_en1,
                      branch_w_en2;

wire    [4 : 0]       fifo_rs1_id,
                      fifo_rs2_id,
                      main_rs1_id,
                      main_rs2_id;

wire    [`rs_width-1 : 0]       rs_ins1, rs_ins2;

dispatch dispatch (
  .clk(clk_in),
  .rst((rst_in | (branch_en & pre_fal)) & dp_en),
  .en(branch_rdy & (~(branch_en & pre_fal)) & dp_en),
  .ins1(de_ins1),
  .ins2(de_ins2),
  .ins1_valid(de_ins1_valid & (~de_ins1[0])),
  .ins2_valid(de_ins2_valid & (~de_ins2[0])),

  .t_reg_data1(reg_data1),
  .t_reg_data2(reg_data2),
  .t_reg_data3(reg_data3),
  .t_reg_data4(reg_data4),
  .t_reg_suc1(reg_suc1),
  .t_reg_suc2(reg_suc2),
  .t_reg_suc3(reg_suc3),
  .t_reg_suc4(reg_suc4),

  .reg_id1(reg_id1),
  .reg_id2(reg_id2),
  .reg_id3(reg_id3),
  .reg_id4(reg_id4),

  .lock_id1(lock_id1),
  .lock_rs1(lock_rs1),
  .lock_id2(lock_id2),
  .lock_rs2(lock_rs2),

  .t_fifo_empty1(fifo_empty1),
  .t_fifo_empty2(fifo_empty2),
  .fifo_rs1_id(fifo_rs1_id),
  .fifo_rs2_id(fifo_rs2_id),
  .t_main_empty1(main_empty1),
  .t_main_empty2(main_empty2),
  .main_rs1_id(main_rs1_id),
  .main_rs2_id(main_rs2_id),
/*
  .branch_rs1_id(5'b10000),
  .branch_empty(branch_empty),
*/
  .fifo_w_en1(fifo_w_en1),
  .fifo_w_en2(fifo_w_en2),
  .main_w_en1(main_w_en1),
  .main_w_en2(main_w_en2),
/*
  .branch_w_en1(branch_w_en1),
  .branch_w_en2(branch_w_en2),
*/
  .rs_ins1(rs_ins1),
  .rs_ins2(rs_ins2),
  .rdy(dp_rdy),
  
  .w_en1(cdb1_en),
  .w_en2(cdb2_en),
  .w_en3(cdb3_en),
  .w_rs1(cdb1_reg_id),
  .w_data1(cdb1_data),
  .w_rs2(cdb2_id),
  .w_data2(cdb2_data),
  .w_rs3(cdb3_id),
  .w_data3(cdb3_data),

  .dp1_suc(dp1_suc),
  .dp2_suc(dp2_suc)
);

wire      fifo_suc, rs_suc, branch_suc;


rs_heap_fifo rs_heap_fifo (
  .clk(clk_in),
  .rst(rst_in),
  .suc(fifo_suc),
  .o_suc(rs_suc),
/*
  .cdb1_en(cdb1_en),
  .cdb1_id(cdb1_id),
  .cdb1_data(cdb1_data),
*/
  .cdb2_en(cdb2_en),
  .cdb2_id(cdb2_id),
  .cdb2_data(cdb2_data),
  .cdb3_en(cdb3_en),
  .cdb3_id(cdb3_id),
  .cdb3_data(cdb3_data),
  .w_en1(fifo_w_en1),
  .w_en2(fifo_w_en2),
  .w_ins1(rs_ins1),
  .w_ins2(rs_ins2),

  .empty1(fifo_empty1),
  .empty2(fifo_empty2),
  .rs_id1(fifo_rs1_id),
  .rs_id2(fifo_rs2_id),
  .rs_id(ls_rs_id),
  .rdy(ls_rs_rdy),
  .ex_ins(ls_rs_ins),
  .in_status(ls_alu_status),
  .data_valid(ls_dat_valid),
  .mem_cfr(ls_mem_cfr)
);

l_s_alu l_s_alu (
  .clk(clk_in),
  .rst(rst_in),

  .en(ls_rs_rdy),
  .ins(ls_rs_ins),
  .rs_id(ls_rs_id),

  .mem_data(ls_mem_data),
  .data_valid(ls_dat_valid),
  .mem_cfr(ls_mem_cfr),
  .mem_add(ls_mem_add),
  .mem_w_data(ls_mem_w_data),
  .mem_w_mask(ls_mem_w_mask),
  .req_sig(ls_req_sig),
  .req_w(ls_req_w),

  .cdb_en(cdb2_en),
  .cdb_id(cdb2_id),
  .cdb_data(cdb2_data),
  .cdb_reg_id(cdb2_reg_id),
  .out_status(ls_alu_status)
);

wire                      cal_rs_rdy;
wire  [4 : 0]             cal_rs_id;
wire  [`rs_width-1 : 0]   cal_rs_ins;

rs_heap_cal rs_heap_cal (
  .clk(clk_in),
  .rst(rst_in),
  .suc(rs_suc),
  .o_suc(fifo_suc),
/*
  .cdb1_en(cdb1_en),
  .cdb1_id(cdb1_id),
  .cdb1_data(cdb1_data),
*/
  .cdb2_en(cdb2_en),
  .cdb2_id(cdb2_id),
  .cdb2_data(cdb2_data),
  .cdb3_en(cdb3_en),
  .cdb3_id(cdb3_id),
  .cdb3_data(cdb3_data),
  .w_en1(main_w_en1),
  .w_en2(main_w_en2),
  .w_ins1(rs_ins1),
  .w_ins2(rs_ins2),

  .empty1(main_empty1),
  .empty2(main_empty2),
  .rs_id1(main_rs1_id),
  .rs_id2(main_rs2_id),
  .rs_id(cal_rs_id),
  .rdy(cal_rs_rdy),
  .ex_ins(cal_rs_ins)
);

main_alu main_alu (
  .clk(clk_in),
  .rst(rst_in),

  .en(cal_rs_rdy),
  .ins(cal_rs_ins),
  .rs_id(cal_rs_id),

  .cdb_en(cdb3_en),
  .cdb_id(cdb3_id),
  .cdb_data(cdb3_data),
  .cdb_reg_id(cdb3_reg_id)
);

wire                      branch_rs_rdy;
wire  [`rs_width-1 : 0]   branch_rs_ins;
wire                      tmp_branch_empty;

//assign branch_suc = branch_empty | ~(branch_w_en1 | branch_w_en2);
/*
rs_unit branch_rs (
  .clk(clk_in),
  .rst(rst_in),
  .cfr(1'b1),
  .o_suc(fifo_suc & rs_suc),

  .cdb1_en(cdb1_en),
  .cdb1_id(cdb1_id),
  .cdb1_data(cdb1_data),
  .cdb2_en(cdb2_en),
  .cdb2_id(cdb2_id),
  .cdb2_data(cdb2_data),
  .cdb3_en(cdb3_en),
  .cdb3_id(cdb3_id),
  .cdb3_data(cdb3_data),
  .w_en1(branch_w_en1),
  .w_en2(branch_w_en2),
  .w_ins1(rs_ins1),
  .w_ins2(rs_ins2),

  .occu(tmp_branch_empty),
  .rdy(branch_rs_rdy),
  .ex_ins(branch_rs_ins)
);
assign branch_empty = ~tmp_branch_empty;
*/

branch_alu branch_alu (
  .clk(clk_in),
  .rst(rst_in | (branch_en & pre_fal) | (~dp_en)),

  .t_en(de_ins1_valid & de_ins1[0] & dp_rdy & dp_en),
  .t_ins(de_ins1[`de_width-1 : 3]),
  .t_rs_id(5'b10000),
  .pre_taken(pc_buff_r_data[18]),
  .in_pc(pc_buff_r_data[17 : 0]),

  .t_rs1(branch_rdata1),
  .t_rs2(branch_rdata2),
  .t_rs1_suc(branch_rsuc1),
  .t_rs2_suc(branch_rsuc2),

  .cdb_en(cdb1_en),
  .cdb_id(cdb1_id),
  .cdb_data(cdb1_data),
  .cdb_reg_id(cdb1_reg_id),

  .branch_en(branch_en),
  .pre_fal(pre_fal),
  .new_pc(pc2),
  .rdy(branch_rdy),
  .w_data(btb_w_data),

  .cdb2_en(cdb2_en),
  .cdb2_id(cdb2_id),
  .cdb2_data(cdb2_data),
  .cdb3_en(cdb3_en),
  .cdb3_id(cdb3_id),
  .cdb3_data(cdb3_data),

  .btb_pc(try_btb_data),

  .p_rdy(p_branch_rdy),
  .p_branch_en(p_branch_en)
);

// implementation goes here

// Specifications:
// - Pause cpu(freeze pc, registers, etc.) when rdy_in is low
// - Memory read takes 2 cycles(wait till next cycle), write takes 1 cycle(no need to wait)
// - Memory is of size 128KB, with valid address ranging from 0x0 to 0x20000
// - I/O port is mapped to address higher than 0x30000 (mem_a[17:16]==2'b11)
// - 0x30000 read: read a byte from input
// - 0x30000 write: write a byte to output (write 0x00 is ignored)
// - 0x30004 read: read clocks passed since cpu starts (in dword, 4 bytes)
// - 0x30004 write: indicates program stop (will output '\0' through uart tx)

/*
always @(posedge clk_in)
  begin
    if (rst_in)
      begin
      
      end
    else if (!rdy_in)
      begin
      
      end
    else
      begin
      
      end
  end
*/

endmodule