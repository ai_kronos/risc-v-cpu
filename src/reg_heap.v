`define data_width 32
module reg_ctr (
    input                       clk,
    input                       rst,

    input                       r_en,
    input   [4 : 0]             r_id1,
    input   [4 : 0]             r_id2,
    input   [4 : 0]             r_id3,
    input   [4 : 0]             r_id4,
    output  [`data_width-1 : 0] r_data1,
    output  [`data_width-1 : 0] r_data2,
    output  [`data_width-1 : 0] r_data3,
    output  [`data_width-1 : 0] r_data4,
    output                      r_suc1,
    output                      r_suc2,
    output                      r_suc3,
    output                      r_suc4,
    input   [4 : 0]             lock_id1,
    input   [4 : 0]             lock_rs1,
    input   [4 : 0]             lock_id2,
    input   [4 : 0]             lock_rs2,

    input                       branch_en,
    input   [4 : 0]             branch_rid1,
    input   [4 : 0]             branch_rid2,
    output                      branch_rsuc1,
    output                      branch_rsuc2,
    output  [`data_width-1 : 0] branch_rdata1,
    output  [`data_width-1 : 0] branch_rdata2,

    input                       w_en1,
    input                       w_en2,
    input                       w_en3,
    input   [4 : 0]             w_id1,
//    input   [4 : 0]             w_rs1,
    input   [`data_width-1 : 0] w_data1,
    input   [4 : 0]             w_id2,
    input   [4 : 0]             w_rs2,
    input   [`data_width-1 : 0] w_data2,
    input   [4 : 0]             w_id3,
    input   [4 : 0]             w_rs3,
    input   [`data_width-1 : 0] w_data3
);
reg     [4 : 0]                 q_branch_rid1, q_branch_rid2;
wire    [31 : 0]                t_branch_rdata1, t_branch_rdata2;

assign branch_rdata1 = (branch_rsuc1)? t_branch_rdata1 : q_lock_name[q_branch_rid1];
assign branch_rdata2 = (branch_rsuc2)? t_branch_rdata2 : q_lock_name[q_branch_rid2];

always@(posedge clk)
begin
    if (rst)
    begin
        q_branch_rid1 <= 'b0;
        q_branch_rid2 <= 'b0;
    end
    else
        if (branch_en)
        begin
            q_branch_rid1 <= branch_rid1;
            q_branch_rid2 <= branch_rid2;
        end
end
assign branch_rsuc1 = ~q_is_lock[q_branch_rid1];
assign branch_rsuc2 = ~q_is_lock[q_branch_rid2];

integer i;
reg     [4 : 0]                 q_lock_name [31 : 0];
reg     [4 : 0]                 d_lock_name [31 : 0];
reg                             q_is_lock [31 : 0];
reg                             d_is_lock [31 : 0];
reg     [4 : 0]                 d_w_id1, d_w_id2, d_w_id3;
wire    [`data_width-1 : 0]     t_r_data1, t_r_data2,
                                t_r_data3, t_r_data4;
reg                             d_r_suc1, d_r_suc2,
                                d_r_suc3, d_r_suc4;
reg     [`data_width-1 : 0]     d_r_data1, d_r_data2,
                                d_r_data3, d_r_data4;

assign r_data1 = d_r_data1;
assign r_data2 = d_r_data2;
assign r_data3 = d_r_data3;
assign r_data4 = d_r_data4;
assign r_suc1 = d_r_suc1;
assign r_suc2 = d_r_suc2;
assign r_suc3 = d_r_suc3;
assign r_suc4 = d_r_suc4;

reg_heap reg_h (
    .clk(clk),
    .rst(rst),
    .r_id1(r_id1),
    .r_id2(r_id2),
    .r_id3(r_id3),
    .r_id4(r_id4),
    .r_id5(q_branch_rid1),
    .r_id6(q_branch_rid2),
    .r_data1(t_r_data1),
    .r_data2(t_r_data2),
    .r_data3(t_r_data3),
    .r_data4(t_r_data4),
    .r_data5(t_branch_rdata1),
    .r_data6(t_branch_rdata2),
    .w_en1(w_en1),
    .w_en2(w_en2),
    .w_en3(w_en3),
    .w_id1(d_w_id1),
    .w_data1(w_data1),
    .w_id2(d_w_id2),
    .w_data2(w_data2),
    .w_id3(d_w_id3),
    .w_data3(w_data3)
);

wire            unlock_w1,
                unlock_w2,
                unlock_w3;

assign unlock_w1 = w_en1;
assign unlock_w2 = (q_lock_name[w_id2] == w_rs2 & w_en2);
assign unlock_w3 = (q_lock_name[w_id3] == w_rs3 & w_en3);

always@*
begin
    d_w_id1 = 'b0;
    d_w_id2 = 'b0;
    d_w_id3 = 'b0;
    d_r_data1 = 'b0;
    d_r_data2 = 'b0;
    d_r_data3 = 'b0;
    d_r_data4 = 'b0;
    d_r_suc1 = 'b0;
    d_r_suc2 = 'b0;
    d_r_suc3 = 'b0;
    d_r_suc4 = 'b0;
    for (i = 0; i < 32; i = i + 1)
    begin
        d_lock_name[i] = q_lock_name[i];
        d_is_lock[i] = q_is_lock[i];
    end
    if (unlock_w1)
    begin
        d_is_lock[w_id1] = 1'b0;
        d_w_id1 = w_id1;
    end
    if (q_is_lock[w_id2] & unlock_w2)
    begin
        d_is_lock[w_id2] = 1'b0;
        d_w_id2 = w_id2;
    end
    if (q_is_lock[w_id3] & unlock_w3)
    begin
        d_is_lock[w_id3] = 1'b0;
        d_w_id3 = w_id3;
    end

    if (r_en)
    begin
        if (~q_is_lock[r_id1])
        begin
            d_r_suc1 = 1'b1;
            d_r_data1 = t_r_data1;
        end
        else
        begin
            d_r_suc1 = 1'b0;
            d_r_data1 = q_lock_name[r_id1];
        end

        if (~q_is_lock[r_id2])
        begin
            d_r_suc2 = 1'b1;
            d_r_data2 = t_r_data2;
        end
        else
        begin
            d_r_suc2 = 1'b0;
            d_r_data2 = q_lock_name[r_id2];
        end


        if (~q_is_lock[r_id3])
        begin
            d_r_suc3 = 1'b1;
            d_r_data3 = t_r_data3;
        end
        else
        begin
            d_r_suc3 = 1'b0;
            d_r_data3 = q_lock_name[r_id3];
        end

        if (~q_is_lock[r_id4])
        begin
            d_r_suc4 = 1'b1;
            d_r_data4 = t_r_data4;
        end
        else
        begin
            d_r_suc4 = 1'b0;
            d_r_data4 = q_lock_name[r_id4];
        end

        d_is_lock[lock_id1] = 1'b1;
        d_lock_name[lock_id1] = lock_rs1;
        d_is_lock[lock_id2] = 1'b1;
        d_lock_name[lock_id2] = lock_rs2;

        d_is_lock[0] = 'b0;
    end
end

always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 32; i = i + 1)
        begin
            q_lock_name[i] <= 'b0;
            q_is_lock[i] <= 'b0;
        end
    end
    else
    begin
        for (i = 0; i < 32; i = i + 1)
        begin
            q_lock_name[i] <= d_lock_name[i];
            q_is_lock[i] <= d_is_lock[i];
        end
    end
end

endmodule

module reg_heap (
    input                       clk,
    input                       rst,

    input   [4 : 0]             r_id1,
    input   [4 : 0]             r_id2,
    input   [4 : 0]             r_id3,
    input   [4 : 0]             r_id4,
    output  [`data_width-1 : 0] r_data1,
    output  [`data_width-1 : 0] r_data2,
    output  [`data_width-1 : 0] r_data3,
    output  [`data_width-1 : 0] r_data4,
    output  [`data_width-1 : 0] r_data5,
    output  [`data_width-1 : 0] r_data6,

    input   [4 : 0]             r_id5,
    input   [4 : 0]             r_id6,

    input                       w_en1,
    input                       w_en2,
    input                       w_en3,
    input   [4 : 0]             w_id1,
    input   [`data_width-1 : 0] w_data1,
    input   [4 : 0]             w_id2,
    input   [`data_width-1 : 0] w_data2,
    input   [4 : 0]             w_id3,
    input   [`data_width-1 : 0] w_data3
);
reg [`data_width-1 : 0]     d_reg [31 : 0];
reg [`data_width-1 : 0]     q_reg [31 : 0];

assign  r_data1 = q_reg[r_id1];
assign  r_data2 = q_reg[r_id2];
assign  r_data3 = q_reg[r_id3];
assign  r_data4 = q_reg[r_id4];
assign  r_data5 = q_reg[r_id5];
assign  r_data6 = q_reg[r_id6];

integer i;
always@*
begin
    for (i = 0; i < 32; i = i + 1)
        d_reg[i] = q_reg[i];

    if ((|w_id1) & w_en1)
        d_reg[w_id1] = w_data1;
    if ((|w_id2) & w_en2)
        d_reg[w_id2] = w_data2;
    if ((|w_id3) & w_en3)
        d_reg[w_id3] = w_data3;
end

always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 32; i = i + 1)
            q_reg[i] <= 'b0;
    end
    else
    begin
        for (i = 0; i < 32; i = i + 1)
            q_reg[i] <= d_reg[i];
    end
end

endmodule