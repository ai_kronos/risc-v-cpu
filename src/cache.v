module mem_ctr #(
    parameter COL       =   2,
              ADD_WIDTH =   18
) (
    input   wire                        clk,
//    input   wire                        clk_mem,
    input   wire                        rst,

    input   wire    [ADD_WIDTH-1 : 0]   iadd,
    input   wire    [ADD_WIDTH-1 : 0]   dadd,
    input   wire                        ireq,
    input   wire                        dreq,
    input   wire                        dwrt,
    input   wire    [(2**COL)*8-1 : 0]  dwr_data,
    output  wire    [(2**COL)*8-1 : 0]  idata_out,//ok
    output  wire    [(2**COL)*8-1 : 0]  ddata_out,//ok
    output  wire                        idata_valid,//ok
    output  wire                        ddata_valid,//ok
    output  wire                        mem_ctr_cfr,//ok

    input   wire                        hreq,
    input   wire                        hbyte,
    input   wire                        hwrt,
    input   wire    [ADD_WIDTH-1 : 0]   hadd,
    input   wire    [31 : 0]            hci_w_data,
    output  wire    [31 : 0]            hci_r_data,
    output  wire                        hci_dat_valid,

    input   wire    [7 : 0]             mem_data,
    output  wire                        mem_wr,//ok
    output  wire    [ADD_WIDTH-1 : 0]   mem_add,//ok
    output  wire    [7 : 0]             mem_w_data//ok
);
wire                        d_mem_valid;

wire    [ADD_WIDTH-1 : 0]   sel_add;
assign sel_add = (hreq)? hadd : dadd;
wire    [(2**COL)*8-1 : 0]  sel_w_data;
assign sel_w_data = (hreq)? hci_w_data : dwr_data;

reg     [(2**COL)*8-1 : 0]  q_data_buffer, d_data_buffer,
                            q_w_data_buff, d_w_data_buff;

reg                         d_hbyte, q_hbyte,
                            d_hreq, q_hreq;

reg     [1 : 0]             q_status, d_status;//0:idle 1:occupied by ins 2:write 3:occupied by dat
reg     [COL - 1 : 0]       q_cnt, d_cnt;
reg                         d_idata_valid, q_idata_valid,
                            d_ddata_valid, q_ddata_valid,
                            d_hci_dat_valid, q_hci_dat_valid,
                            q_mem_valid,
                            d_cfr, q_cfr,
                            d_mem_wr, q_mem_wr;
reg     [ADD_WIDTH-1 : 0]   d_add, q_add;
reg d_debug;

//assign  d_mem_valid = (clk_mem & ~q_mem_valid);
assign d_mem_valid = 1'b1;

always@*
begin
    d_data_buffer = q_data_buffer;
    d_w_data_buff = q_w_data_buff;
    d_hbyte = q_hbyte;
    d_hreq = q_hreq;
    d_cfr = q_cfr;
    d_add = q_add;
    d_mem_wr = 1'b0;
    d_debug = 1'b0;
    d_hci_dat_valid = 1'b0;
    d_idata_valid = 'b0;
    d_ddata_valid = 'b0;

    if (|q_cnt)
        d_data_buffer[q_cnt*8-8 +: 8] = mem_data;
    else
        d_data_buffer[24 +: 8] = mem_data;

    case (q_status)
        2'h0:
        begin
            d_cnt = 'b0;
            case ({ireq, (dreq | hreq)})
                2'h0:
                begin
                    d_status = 2'h0;
                    d_hbyte = 1'b0;
                end
                2'h2:
                begin
                    d_cfr = 1'b0;
                    d_status = 2'h1;
                    d_add = iadd;
                    d_hbyte = 1'b0;
                end
                default:
                begin
                    d_hbyte = hbyte;
                    d_hreq = hreq;
                    d_add = sel_add;
                    d_cfr = 1'b0;
                    if ((dreq & dwrt & ~hreq) | (hreq & hwrt))
                    begin
                        d_mem_wr = 1'b1;
                        d_w_data_buff = sel_w_data;
                        d_status = 2'h2;
                    end
                    else
                        d_status = 2'h3;
                end
            endcase
        end
        2'h1:
        begin
            d_cnt = q_cnt + 1'b1;
            
            if (((&q_cnt) | (q_hbyte & q_hreq)) & d_mem_valid)
            begin
                d_idata_valid = 'b1;
                d_status = 'b0;
                d_cfr = 1'b1;
//                d_cnt = 'b0;
                d_hreq = 'b0;
                d_add = 'b0;
            end
            else
                d_status = 2'h1;
        end
        2'h2:
        begin
            d_cnt = q_cnt + 1'b1;
            d_mem_wr = 1'b1;

            if (((&q_cnt) | (q_hbyte & q_hreq)) & d_mem_valid)
            begin
                d_status = 'b0;
                d_mem_wr = 'b0;
//                d_cnt = 'b0;
                d_cfr = 1'b1;
                d_hreq = 'b0;
                d_add = 'b0;
            end
            else
                d_status = 2'h2;
        end
        default:
        begin
            d_cnt = q_cnt + 1'b1;

            if (((&q_cnt) | (q_hbyte & q_hreq)) & d_mem_valid)
            begin
                d_status = 'b0;
//                d_cnt = 'b0;
                d_cfr = 1'b1;
                d_hreq = 'b0;
                d_add = 'b0;
                if (q_hreq)
                    d_hci_dat_valid = 1'b1;
                else
                    d_ddata_valid = 1'b1;
            end
            else
                d_status = 2'h3;
        end
    endcase
end

always@(posedge clk)
begin
    if (rst)
    begin
        q_status        <= 2'h0;
//        q_mem_valid     <= 1'b0;
        q_cnt           <= 2'h0;
        q_data_buffer   <= 'b0;
        q_w_data_buff   <= 'b0;
        q_mem_wr        <= 1'b0;
        q_add           <= 'b0;
        q_cfr           <= 'b1;
        q_hbyte         <= 1'b0;
        q_hreq          <= 'b0;
        q_ddata_valid   <= 'b0;
        q_idata_valid   <= 'b0;
        q_hci_dat_valid <= 'b0;
    end
    else
    begin
//        q_mem_valid     <= clk_mem;
        q_ddata_valid   <= d_ddata_valid;
        q_idata_valid   <= d_idata_valid;
        q_hci_dat_valid <= d_hci_dat_valid;
        q_status        <= d_status;
        q_cnt           <= d_cnt;
        q_data_buffer   <= d_data_buffer;
        q_mem_wr        <= d_mem_wr;
        q_add           <= d_add;
        q_cfr           <= d_cfr;
        q_w_data_buff   <= d_w_data_buff;
        q_hbyte         <= d_hbyte;
        q_hreq          <= d_hreq;
    end
end

assign idata_valid      = q_idata_valid;
assign ddata_valid      = q_ddata_valid;
assign idata_out        = d_data_buffer;
assign ddata_out        = d_data_buffer;
assign hci_r_data       = d_data_buffer;
assign hci_dat_valid    = q_hci_dat_valid;
assign mem_ctr_cfr      = q_cfr;

assign mem_wr           = q_mem_wr;
assign mem_add          = {q_add[ADD_WIDTH-1 : COL], q_cnt};
assign mem_w_data       = q_w_data_buff[q_cnt*8 +: 8];

endmodule

module ins_cache #(
    parameter ADD_WIDTH = 18,
    parameter COL       =  2,
    parameter LINE      =  6
) (
    input   wire                        clk,
    input   wire                        rst,
    output  wire                        in_rdy,//ok
    input   wire    [ADD_WIDTH-1 : 0]   add,
    input   wire                        en,
    output  wire    [(2**COL)*8-1 : 0]  data,//ok
    output  wire    [(2**COL)*8-1 : 0]  try_data,//ok
    output  wire                        out_rdy,//ok
    output  wire                        try_out_rdy,

    input   wire                        t_mem_rdy,
    input   wire    [(2**COL)*8-1 : 0]  mem_data,
    input   wire                        mem_dat_valid,
    output  wire    [ADD_WIDTH-1 : 0]   addr,//ok
    output  wire                        req_sig,//ok

    input   wire                        dat_req_sig,
    input   wire                        hci_req_sig
);

wire    [ADD_WIDTH-1 : 0]   try_add;
assign          try_add = add + 3'h4;

wire                        mem_rdy;
wire                        hit;
wire    [1 : 0]             ca_hit;
wire    [1 : 0]             try_ca_hit;
wire                        d_out_rdy, d_try_out_rdy;
wire                        d_in_rdy;
wire    [ADD_WIDTH-1 : 0]   d_add;
wire    [(2**COL)*8-1 : 0]  d_data, d_try_data;

reg     [1 : 0]             q_status, d_status,//0:idle 1:waiting for idle of mem_ctr 2:wating for data from mem
                            d_hit;
reg                         q_in_rdy,
                            q_out_rdy,
                            q_try_out_rdy,
                            d_req_sig;
reg     [ADD_WIDTH-1 : 0]   q_add;
reg     [(2**COL)*8-1 : 0]  q_data, q_try_data;

//two cache block
wire    [1 : 0]                     en_w;
wire    [(2**COL)*8-1 : 0]          ca_data     [1 : 0];
wire    [ADD_WIDTH-COL-LINE-1 : 0]  ca_hi_add   [1 : 0];
wire                                ca_valid    [1 : 0];
wire    [(2**COL)*8-1 : 0]          try_ca_data     [1 : 0];
wire    [ADD_WIDTH-COL-LINE-1 : 0]  try_ca_hi_add   [1 : 0];
wire                                try_ca_valid    [1 : 0];

dir_cache_block #(.ADD_WIDTH(ADD_WIDTH),
                  .COL(COL),
                  .LINE(LINE)) bcache0 (
    .clk(clk),
    .rst(rst),
    .add(add),
    .try_add(try_add),
    .add_w(q_add),
    .en_w(en_w[0]),
    .data_w(d_data),
    .data(ca_data[0]),
    .hi_add_out(ca_hi_add[0]),
    .valid_out(ca_valid[0]),
    .try_data(try_ca_data[0]),
    .try_hi_add_out(try_ca_hi_add[0]),
    .try_valid_out(try_ca_valid[0])
);

dir_cache_block #(.ADD_WIDTH(ADD_WIDTH),
                  .COL(COL),
                  .LINE(LINE)) bcache1 (
    .clk(clk),
    .rst(rst),
    .add(add),
    .try_add(try_add),
    .add_w(q_add),
    .en_w(en_w[1]),
    .data_w(d_data),
    .data(ca_data[1]),
    .hi_add_out(ca_hi_add[1]),
    .valid_out(ca_valid[1]),
    .try_data(try_ca_data[1]),
    .try_hi_add_out(try_ca_hi_add[1]),
    .try_valid_out(try_ca_valid[1])
);
//last used
reg     [1 : 0] last_used     [2**LINE - 1 : 0];

//logic
wire    [ADD_WIDTH-COL-LINE-1 : 0]  hi_add, try_hi_add;
assign  hi_add        = add[ADD_WIDTH - 1 : COL + LINE];
assign  try_hi_add    = try_add[ADD_WIDTH - 1 : COL + LINE];


assign  mem_rdy = t_mem_rdy & (~dat_req_sig) & (~hci_req_sig);
assign  d_add = (q_in_rdy)? add : q_add;
assign  ca_hit[0] = (ca_hi_add[0] == hi_add & ca_valid[0]);
assign  ca_hit[1] = (ca_hi_add[1] == hi_add & ca_valid[1]);

assign  try_ca_hit[0] = (try_ca_hi_add[0] == try_hi_add && try_ca_valid[0]);
assign  try_ca_hit[1] = (try_ca_hi_add[1] == try_hi_add && try_ca_valid[1]);
assign  try_out_rdy = q_try_out_rdy;
assign  d_try_out_rdy = (|try_ca_hit) & en & hit;
assign  d_try_data = (try_ca_hit[1])? try_ca_data[1] : try_ca_data[0];
assign  try_data = q_try_data;

assign  hit = (|ca_hit) & q_in_rdy & en;
assign  d_out_rdy = hit | mem_dat_valid;
assign  d_in_rdy = (~rst) & (d_out_rdy | (q_in_rdy & ~en));
//assign  en_w = (q_out_rdy && q_add[ADD_WIDTH - 1 : ADD_WIDTH - 2] != 2'h3)? last_used[q_add[LINE + COL - 1 : COL]] : 2'h0;
assign  en_w = (mem_dat_valid)? last_used[d_add[LINE + COL - 1 : COL]] : 2'h0;
assign  d_data = ((mem_dat_valid)? mem_data : ((ca_hit[1])? ca_data[1] : ca_data[0]));

assign  out_rdy = q_out_rdy;
assign  in_rdy = q_in_rdy;
assign  data = q_data;

reg q_req_sig;

assign  addr = d_add;
assign  req_sig = d_req_sig;

always@*
begin
    if (rst)
    begin
        d_hit = 2'h1;
        d_req_sig = 1'b1;
        d_status = 2'h0;
    end
    else
    begin
        d_hit = last_used[add[LINE + COL - 1 : COL]];
        case (q_status) 
            2'h0:
            begin
                if (en & hit)
                    d_hit = ca_hit;

                if (en & ~hit)
                begin
                    d_hit = ~last_used[add[LINE + COL - 1 : COL]];
                    d_req_sig = 1'b1;
                    if (mem_rdy)
                        d_status = 2'h2;
                    else
                        d_status = 2'h1;
//                    d_status = 2'h1;
                end
                else
                begin
                    d_status = 2'h0;
                    d_req_sig = 1'b0;
                end
            end
            2'h1:
            begin
                if (mem_rdy)
                begin
//                    d_req_sig = 1'b0;
                    d_req_sig = 1'b1;
                    d_status = 2'h2;
                end
                else
                begin
                    d_status = 2'h1;
                    d_req_sig = 1'b1;
                end
            end
            default: 
            begin
                d_req_sig = 1'b0;
                if (mem_dat_valid)
                begin
                    d_status = 2'h0;
                end
                else
                    d_status = 2'h2;
            end
        endcase
    end
end

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        q_out_rdy   <= 1'b0;
        q_try_out_rdy <= 1'b0;
        q_status    <= 2'h0;
        q_in_rdy    <= 1'b1;
        q_add       <=  'b0;
        q_data      <=  'b0;
        q_data      <=  'b0;
        q_try_data  <=  'b0;
        q_req_sig   <=  'b0;

        for (i = 0; i < 2**LINE; i = i + 1)
            last_used[i]    <= 2'h1;
    end
    else
    begin
        q_req_sig                               <= d_req_sig;
        q_try_out_rdy                           <= d_try_out_rdy;
        q_out_rdy                               <= d_out_rdy;
        q_status                                <= d_status;
        q_in_rdy                                <= d_in_rdy;
        q_add                                   <= d_add;
        q_data                                  <= d_data;
        q_try_data                              <= d_try_data;
        last_used   [add[LINE + COL - 1 : COL]] <= d_hit;
    end
end
endmodule

module dir_cache_block #(
    parameter ADD_WIDTH = 18,
    parameter COL       =  2,
    parameter LINE      =  7
) (
    input   wire                                clk,
    input   wire                                rst,
    input   wire    [ADD_WIDTH-1 : 0]           add,
    input   wire    [ADD_WIDTH-1 : 0]           add_w,
    input   wire                                en_w,
    input   wire    [(2**COL)*8-1 : 0]          data_w,
    input   wire    [ADD_WIDTH-1 : 0]           try_add,
    output  wire    [(2**COL)*8-1 : 0]          data,
    output  wire    [ADD_WIDTH-COL-LINE-1 : 0]  hi_add_out,
    output  wire                                valid_out,
    output  wire    [(2**COL)*8-1 : 0]          try_data,
    output  wire    [ADD_WIDTH-COL-LINE-1 : 0]  try_hi_add_out,
    output  wire                                try_valid_out
);
//stock
reg                                 valid   [2**LINE - 1 : 0];
reg     [ADD_WIDTH-COL-LINE-1 : 0]  hi_add  [2**LINE - 1 : 0];
reg     [(2**COL)*8-1 : 0]          dat     [2**LINE - 1 : 0];

//logic
wire    [LINE - 1 : 0]              line_num;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  d_hi_add;
wire    [LINE - 1 : 0]              line_num_w;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  d_hi_add_w;

wire    [LINE - 1 : 0]              try_line_num;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  try_d_hi_add;

assign  line_num        = add[LINE + COL - 1 : COL];
assign  d_hi_add        = add[ADD_WIDTH - 1 : COL + LINE];
assign  line_num_w      = add_w[LINE + COL - 1 : COL];
assign  d_hi_add_w      = add_w[ADD_WIDTH - 1 : COL + LINE];

assign  try_line_num    = try_add[LINE + COL - 1 : COL];
assign  try_d_hi_add    = try_add[ADD_WIDTH - 1 : COL + LINE];

assign  data            = dat[line_num];
assign  hi_add_out      = hi_add[line_num];
assign  valid_out       = valid[line_num];

assign  try_data        = dat[try_line_num];
assign  try_hi_add_out  = hi_add[try_line_num];
assign  try_valid_out   = valid[try_line_num];

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 2**LINE; i = i + 1)
        begin
            valid[i] <= 1'b0;
            dat[i]   <=  'b0;
        end
    end
    else if (en_w)
        begin
            valid[line_num_w]   <= 1'b1;
            dat[line_num_w]     <= data_w;
            hi_add[line_num_w]  <= d_hi_add_w;
        end
end
endmodule

module dir_dat_cache_block #(
    parameter ADD_WIDTH = 18,
    parameter COL       =  2,
    parameter LINE      =  7
) (
    input   wire                                clk,
    input   wire                                rst,
    input   wire    [ADD_WIDTH-1 : 0]           add,
    input   wire    [ADD_WIDTH-1 : 0]           add_w,
    input   wire                                en_w,
    input   wire                                dirty_w,
    input   wire    [(2**COL)*8-1 : 0]          data_w,
    input   wire    [(2**COL)*8-1 : 0]          mask,
    output  wire    [(2**COL)*8-1 : 0]          data,
    output  wire    [ADD_WIDTH-COL-LINE-1 : 0]  hi_add_out,
    output  wire                                valid_out,
    output  wire                                dirty_out
);
//stock
reg                                 valid   [2**LINE - 1 : 0];
reg                                 dirty   [2**LINE - 1 : 0];
reg     [ADD_WIDTH-COL-LINE-1 : 0]  hi_add  [2**LINE - 1 : 0];
reg     [(2**COL)*8-1 : 0]          dat     [2**LINE - 1 : 0];

//logic
wire    [LINE - 1 : 0]              line_num;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  d_hi_add;
wire    [LINE - 1 : 0]              line_num_w;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  d_hi_add_w;

assign  line_num        = add[LINE + COL - 1 : COL];
assign  d_hi_add        = add[ADD_WIDTH - 1 : COL + LINE];
assign  line_num_w      = add_w[LINE + COL - 1 : COL];
assign  d_hi_add_w      = add_w[ADD_WIDTH - 1 : COL + LINE];

assign  data            = dat[line_num];
assign  hi_add_out      = hi_add[line_num];
assign  valid_out       = valid[line_num];
assign  dirty_out       = dirty[line_num];

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        for (i = 0; i < 2**LINE; i = i + 1)
        begin
            valid[i] <= 1'b0;
            dirty[i] <= 1'b0;
            dat[i]   <=  'b0;
        end
    end
    else if (en_w)
        begin
            valid[line_num_w]   <= 1'b1;
            dirty[line_num_w]   <= dirty_w;
            dat[line_num_w]     <= (data_w & mask) | (dat[line_num_w] & (~mask));
            hi_add[line_num_w]  <= d_hi_add_w;
        end
end
endmodule

module dat_cache #(
    parameter ADD_WIDTH = 18,
    parameter COL       =  2,
    parameter LINE      =  6
) (
    input   wire                        clk,
    input   wire                        rst,
    output  wire                        in_rdy,//ok
    input   wire    [ADD_WIDTH-1 : 0]   add,
    input   wire                        en,
    input   wire                        en_w,//new
    input   wire    [(2**COL)*8-1 : 0]  data_w,//new
    input   wire    [(2**COL)*8-1 : 0]  mask,//new
    output  wire    [(2**COL)*8-1 : 0]  data,//ok
    output  wire                        out_rdy,//ok

    input   wire                        t_mem_rdy,
    input   wire    [(2**COL)*8-1 : 0]  mem_data,
    input   wire                        mem_dat_valid,
    output  wire    [ADD_WIDTH-1 : 0]   addr,//ok
    output  wire                        req_sig,//ok
    output  wire                        mem_w_req,//ok
    output  wire    [(2**COL)*8-1 : 0]  mem_w_data,//ok

    input   wire                        hci_req_sig
);

wire                        mem_rdy;
assign  mem_rdy = t_mem_rdy & (~hci_req_sig);

wire                        hit;
wire    [1 : 0]             ca_hit;

reg     [1 : 0]             q_status, d_status,//0:idle 1:wait 2:waiting for data from mem 3:waiting for writing back
                            d_hit,
                            d_hit2, q_hit2;
reg                         q_in_rdy, d_in_rdy,
                            q_out_rdy, d_out_rdy,
                            d_req_sig,
                            d_mem_w_req,
                            d_dirty_w,
                            q_need_wb, d_need_wb,
                            d_ca_w_rdy, q_ca_w_rdy;
reg     [ADD_WIDTH-1 : 0]   q_add, d_add,
                            q_wb_add, d_wb_add;
reg     [(2**COL)*8-1 : 0]  q_data, d_data,
                            cw_data,
                            d_wrt_buff, q_wrt_buff;

wire     [(2**COL)*8-1 : 0] d_mask;

assign d_mask = ((~(|q_status)) & en & en_w & hit)? mask : 32'hffffffff;

//two cache block
wire    [1 : 0]                     en_ca_w,
                                    ca_dirty;
wire    [(2**COL)*8-1 : 0]          ca_data     [1 : 0],
                                    wb_sel;
wire    [ADD_WIDTH-COL-LINE-1 : 0]  ca_hi_add   [1 : 0];
wire                                ca_valid    [1 : 0];

dir_dat_cache_block #(.ADD_WIDTH(ADD_WIDTH),
                  .COL(COL),
                  .LINE(LINE)) bcache0 (
    .clk(clk),
    .rst(rst),
    .add(add),//ok
    .add_w(d_add),//ok
    .en_w(en_ca_w[0]),//ok
    .dirty_w(d_dirty_w),//ok
    .data_w(cw_data),//ok
    .data(ca_data[0]),
    .mask(d_mask),
    .hi_add_out(ca_hi_add[0]),
    .valid_out(ca_valid[0]),
    .dirty_out(ca_dirty[0])
);

dir_dat_cache_block #(.ADD_WIDTH(ADD_WIDTH),
                  .COL(COL),
                  .LINE(LINE)) bcache1 (
    .clk(clk),
    .rst(rst),
    .add(add),
    .add_w(d_add),
    .en_w(en_ca_w[1]),
    .dirty_w(d_dirty_w),
    .data_w(cw_data),
    .data(ca_data[1]),
    .mask(d_mask),
    .hi_add_out(ca_hi_add[1]),
    .valid_out(ca_valid[1]),
    .dirty_out(ca_dirty[1])
);
//last used
reg     [1 : 0] last_used     [2**LINE - 1 : 0];

//logic
wire    [ADD_WIDTH-COL-LINE-1 : 0]  hi_add;
assign  hi_add        = add[ADD_WIDTH - 1 : COL + LINE];

assign  ca_hit[0] = (ca_hi_add[0] == hi_add && ca_valid[0]);
assign  ca_hit[1] = (ca_hi_add[1] == hi_add && ca_valid[1]);
assign  hit = (|ca_hit) & q_in_rdy & en;

//assign  en_ca_w = (q_ca_w_rdy && q_add[ADD_WIDTH - 1 : ADD_WIDTH - 2] != 2'h3)? last_used[q_add[LINE + COL - 1 : COL]] : 2'h0;
assign  en_ca_w = (d_ca_w_rdy)? ~d_hit2 : 2'h0;
assign  out_rdy = q_out_rdy;
assign  in_rdy = q_in_rdy;
assign  data = q_data;

assign  addr = (mem_w_req)? d_wb_add : d_add;
assign  req_sig = d_req_sig;
assign  mem_w_req = d_mem_w_req;
assign  mem_w_data = d_wrt_buff;

always@*
begin
    d_hit = last_used[add[LINE + COL - 1 : COL]];
    d_wrt_buff = q_wrt_buff;
    d_wb_add = q_wb_add;

    if ((~(|q_status)) & en)
    begin
        if (hit)
        begin
            d_hit = ca_hit;
        end
        else
        begin
            d_hit = ~last_used[add[LINE + COL - 1 : COL]];

            if (d_hit & ca_dirty)
            begin
                case (1'b0)
                    last_used[add[LINE + COL - 1 : COL]][0]:
                    begin
                        d_wrt_buff = ca_data[0];
                        d_wb_add = {ca_hi_add[0], add[COL+LINE-1 : 0]};
                    end
                    last_used[add[LINE + COL - 1 : COL]][1]:
                    begin
                        d_wrt_buff = ca_data[1];
                        d_wb_add = {ca_hi_add[1], add[COL+LINE-1 : 0]};
                    end
                    default: begin end
                endcase
            end
        end
    end
end

always@*
begin
    d_hit2 = q_hit2;
    d_need_wb = q_need_wb;
    d_in_rdy = 1'b0;
    d_out_rdy = 1'b0;
    d_data = 'b0;
    d_req_sig = 1'b0;
    d_mem_w_req = 1'b0;
    d_dirty_w = 1'b0;
    d_ca_w_rdy = 1'b0;
    cw_data = 'b0;
    if (q_in_rdy)
        d_add = add;
    else
        d_add = q_add;

    case (q_status)
        2'h0:
        begin
            d_hit2 = last_used[add[LINE + COL - 1 : COL]];
            d_need_wb = 1'b0;
            if (en)
            begin
                if (en_w)
                begin//write
                    d_dirty_w = 1'b1;
                    d_data = data_w;
                    cw_data = data_w;
                    d_ca_w_rdy = 1'b1;
                    if (hit)
                    begin
                        d_status = 2'h0;
                        d_in_rdy = 1'b1;
                        d_hit2 = ~ca_hit;
                    end
                    else
                    begin
                        if (d_hit & ca_dirty)
                        begin
                            d_req_sig = 1'b1;
                            d_mem_w_req = 1'b1;

                            if (mem_rdy)
                            begin
                                d_status = 2'h0;
                                d_in_rdy = 1'b1;
                            end
                            else
                            begin
                                d_status = 2'h3;
                            end
                        end
                        else
                        begin
                            d_status = 2'h0;
                            d_in_rdy = 1'b1;
                        end
                    end
                end
                else
                begin//read
                    if (hit)
                    begin
                        d_hit2 = ~last_used[add[LINE + COL - 1 : COL]];
                        d_status = 2'h0;
                        d_in_rdy = 1'b1;
                        d_out_rdy = 1'b1;
                        if (ca_hit[0])
                            d_data = ca_data[0];
                        else
                            d_data = ca_data[1];
                    end
                    else
                    begin
                        d_req_sig = 1'b1;

                        if(d_hit & ca_dirty)
                        begin
                            d_need_wb = 1'b1;
                        end

                        if (mem_rdy)
                        begin
                            d_status = 2'h2;
                        end
                        else
                        begin
                            d_status = 2'h1;
                        end
                    end
                end
            end
            else
            begin
                d_status = 2'h0;
                d_in_rdy = 1'b1;
            end
        end
        2'h1:
        begin
            d_req_sig = 1'b1;
            if (mem_rdy)
            begin
                d_status = 2'h2;
            end
            else
            begin
                d_status = 2'h1;
            end
        end
        2'h2:
        begin
            if (mem_dat_valid)
            begin
                d_data = mem_data;
                cw_data = mem_data;
                d_out_rdy = 1'b1;
                d_ca_w_rdy = 1'b1;
                if (q_need_wb)
                begin
                    d_req_sig = 1'b1;
                    d_mem_w_req = 1'b1;
                    if (mem_rdy)
                    begin
                        d_status = 2'h0;
                        d_in_rdy = 1'b1;
                    end
                    else
                    begin
                        d_status = 2'h3;
                    end
                end
                else
                begin
                    d_status = 2'h0;
                    d_in_rdy = 1'b1;
                end
            end
            else
            begin
                d_status = 2'h2;
            end
        end
        default:
        begin
            d_req_sig = 1'b1;
            d_mem_w_req = 1'b1;
            if (mem_rdy)
            begin
                d_status = 2'h0;
                d_in_rdy = 1'b1;
            end
            else
            begin
                d_status = 2'h3;
            end
        end
    endcase
end

integer i;

always@(posedge clk)
begin
    if (rst)
    begin
        q_out_rdy   <= 1'b0;
        q_status    <= 2'h0;
        q_in_rdy    <= 1'b1;
        q_add       <=  'b0;
        q_data      <=  'b0;
        q_wrt_buff  <=  'b0;
        q_wb_add    <=  'b0;
        q_ca_w_rdy  <=  'b0;
        q_hit2      <= 2'h1;
        q_need_wb   <= 1'b0;

        for (i = 0; i < 2**LINE; i = i + 1)
            last_used[i]    <= 2'h1;
    end
    else
    begin
        q_out_rdy                               <= d_out_rdy;
        q_status                                <= d_status;
        q_in_rdy                                <= d_in_rdy;
        q_add                                   <= d_add;
        q_data                                  <= d_data;
        last_used   [add[LINE + COL - 1 : COL]] <= d_hit;
        q_wrt_buff                              <= d_wrt_buff;
        q_wb_add                                <= d_wb_add;
        q_ca_w_rdy                              <= d_ca_w_rdy;
        q_hit2                                  <= d_hit2;
        q_need_wb                               <= d_need_wb;
    end
end
endmodule